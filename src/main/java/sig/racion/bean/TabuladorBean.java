/* Utilizado Alexis*/

 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.racion.bean;

import sig.login.bo.AdminUsuarioBO;
import sig.racion.bo.TabuladorBO;
import sig.login.entity.SigCentro;
import sig.racion.entity.SigTabulador;
import sig.login.clase.Mensajes;
import sig.racion.dto.SigTabuladorDTO;
import sig.racion.entity.SigBeneficiario;
import sig.racion.entity.SigTiempocomida;
import sig.login.bean.SessionBean;
import java.util.Date;
import org.primefaces.context.RequestContext;
import static sig.login.clase.ConstantesSistema.ERROR_SISTEMA;
import static sig.login.clase.ConstantesSistema.RUTA_ARCH_VARIOS_SUBIDOS_TEMP;
import sig.login.clase.SubirArchivoServer;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import jxl.Cell;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.primefaces.PrimeFaces;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author Alexis
 */
@ManagedBean
@ViewScoped
@Getter
@Setter
@NoArgsConstructor

public class TabuladorBean extends Mensajes {

    @ManagedProperty(value = "#{tabuladorBO}")
    private TabuladorBO tabuladorBO;

    @ManagedProperty(value = "#{adminUsuarioBO}")
    private AdminUsuarioBO centro;

    @ManagedProperty(value = "#{sessionBean}")
    private SessionBean sessionbean;

    private SigTabulador tab;
    private SigBeneficiario beneficiario;
    private SigTiempocomida tComida;
    private List<SigTabulador> tabuladores;
    private SigCentro centroSig;
    private SigCentro centroUsu;
    private List<SigCentro> centros;
    private List<SigTabuladorDTO> desayunoList;
    private List<SigTabuladorDTO> almuerzoList;
    private List<SigTabuladorDTO> cenaList;
    private List<SigTabuladorDTO> refrigerioList;

    private SigTiempocomida tiComidaEx;
    private SigBeneficiario beneficiarioEx;

    /*Subir Excel*/
    private SigTabulador objTabEx;
    private List<SigTabulador> listTabEx;
    private SubirArchivoServer archivo;
    private String urlArchivo;
    private XSSFWorkbook workbook;
    private XSSFSheet sheet;
    private XSSFRow row;
    private Cell cell;
    private int numFilas;
    private Integer fila;
    private Integer columna;

    @PostConstruct
    public void main() {
        try {
            tab = new SigTabulador();
            centroUsu = new SigCentro();
            centros = centro.listCentro();
            beneficiario = new SigBeneficiario();
            tComida = new SigTiempocomida();
            desayunoList = new ArrayList<>();

            tabuladores = tabuladorBO.tabuladores();
            centroUsu = centro.buscarCentro(sessionbean.getUsu().getSigCentro().getPkIdCentro());
            tab.setSigBeneficiario(beneficiario);
            tab.setSigTcomida(tComida);
        } catch (Exception e) {
            System.out.println("error al construir objetos");
        }

    }

    //Método de guardado todos los centros
    public void guardarTabulador() {
        RequestContext reqestContext = RequestContext.getCurrentInstance();
        try {
            if (tab.getIdTabulador() == null) {
                tab.setTotal(beneficiario.getPersonal() + beneficiario.getHombres() + beneficiario.getMujeres() + beneficiario.getPnc() + beneficiario.getNiños() + beneficiario.getNiñas() + beneficiario.getAncianos() + beneficiario.getAncianas());
                tab.setSigBeneficiario(beneficiario);
                tab.setSigTcomida(tComida);
                tab.setSigCentro(centro.buscarCentro(sessionbean.getUsu().getSigCentro().getPkIdCentro()));
                tabuladorBO.guardarTabulador(tab, sessionbean);
                mensaje(1, "Registro agregado correctamente");
                reqestContext.execute("PF('agregarRegistro').hide()");
                tab = new SigTabulador();
            } else {
                tabuladorBO.actualizarTabulador(tab, sessionbean);
                mensaje(1, "Registro actualizado correctamente");
            }
        } catch (Exception e) {

            mensaje(2, "Error al Agregar registro");
        }
    }

    /*SUBIR ARCHIVO*/
    public void subirArchivo(FileUploadEvent event) {
        try {
            archivo = new SubirArchivoServer();
            urlArchivo = archivo.SubirArchivo(event, RUTA_ARCH_VARIOS_SUBIDOS_TEMP, "TabuladorTemp"); //SUBIR EL ARCHIVO
            PrimeFaces.current().executeScript("PF('widAddExcel').hide()");
            cargarDatos();
        } catch (Exception e) {
            ERROR_SISTEMA("TabuladorBean.subirArchivo", e);
        }
    }

    public void cargarDatos() {
        try {
            /*LEER EL ARCHIVO*/
            //Seleccionamos la hoja que vamos a leer
            sheet = new XSSFWorkbook(new File(urlArchivo)).getSheetAt(0);

            if (comprobarArchivoExcel()) { //COMPROBAR QUE EL ARCHIVO BIEN CON LA ESTRUCTURA    
                if (listTabEx.size() > 0) {

                    if (tabuladorBO.insertTabList(listTabEx, sessionbean)) {
                        mensaje(1, "Raciones servidas guardadas correctamente");
                    } else {
                        mensaje(2, "Error al guardar las raciones servidas");
                    }
                } else {
                    mensaje(2, "Error el excel esta vacio");
                }
            }
            //PrimeFaces.current().ajax().update("formTabulador");
        } catch (IOException | InvalidFormatException e) {
            ERROR_SISTEMA("TabuladorBean.cargarDatos", e);
        }
    }

//    public boolean comprobarArchivoExcel(Sheet sheet) {
    public boolean comprobarArchivoExcel() {
        listTabEx = new ArrayList<>();
        try {
            //EMPESAMOS DESDE LA FILA 2(CON LAS CABECERAS EN LA FILA 1)
            //EL ENCABEZADO DEBE DE ESTAR EN LA FILA 1 
            for (fila = 1; fila <= sheet.getLastRowNum() ; fila++) { //recorremos las filas
            //fila=1;
                objTabEx = new SigTabulador();
                beneficiarioEx = new SigBeneficiario();
                tiComidaEx = new SigTiempocomida();
                for (columna = 0; columna <= 9; columna++) { //recorremos las columnas
                    switch (columna) {

                        case 0://Fecha (Date)
                            if (!sheet.getRow(fila).getCell(columna).getStringCellValue().isEmpty()) {
                                objTabEx.setFecha(new Date(sheet.getRow(fila).getCell(columna).getStringCellValue()));
                            } else {
                                mensaje(2, "Datos no cargados error en la fila " + (fila + 1) + " y columna " + (columna + 1) + " ");
                                mensaje(2, "Verificar el archivo y volver a cargar los datos");
                                return false;
                            }
                            break;
                        case 1://Tiempo de comida (TEXTO)
                            if (!sheet.getRow(fila).getCell(columna).getStringCellValue().isEmpty()) {
                                if (sheet.getRow(fila).getCell(columna).getStringCellValue().toUpperCase().equals("DESAYUNO")) {
                                    tiComidaEx.setDesayuno(Boolean.TRUE);
                                }
                                if (sheet.getRow(fila).getCell(columna).getStringCellValue().toUpperCase().equals("ALMUERZO")) {
                                    tiComidaEx.setAlmuerzo(Boolean.TRUE);
                                }
                                if (sheet.getRow(fila).getCell(columna).getStringCellValue().toUpperCase().equals("CENA")) {
                                    tiComidaEx.setCena(Boolean.TRUE);
                                }
                                if (sheet.getRow(fila).getCell(columna).getStringCellValue().toUpperCase().equals("REFRIGERIO")) {
                                    tiComidaEx.setRefrigerio(Boolean.TRUE);
                                }
                                objTabEx.setSigTcomida(tiComidaEx);
                            } else {
                                mensaje(2, "Datos no cargados error en la fila " + (fila + 1) + " y columna " + (columna + 1) + " ");
                                mensaje(2, "Verificar el archivo y volver a cargar los datos");
                                return false;
                            }
                            break;
                        case 2://Raciones Personal (Int)
                            if (sheet.getRow(fila).getCell(columna).getNumericCellValue() != 0) {
                                //Cell cell = sheet.getRow(fila).getCell(columna);
                                beneficiarioEx.setPersonal((int) sheet.getRow(fila).getCell(columna).getNumericCellValue());
                            } else {
                                mensaje(2, "Datos no cargados error en la fila " + (fila + 1) + " y columna " + (columna + 1) + " ");
                                mensaje(2, "Verificar el archivo y volver a cargar los datos");
                                return false;
                            }
                            break;
                        case 3://Raciones Hombres (Int)
                            if (sheet.getRow(fila).getCell(columna).getNumericCellValue() != 0) {
                                beneficiarioEx.setHombres((int) sheet.getRow(fila).getCell(columna).getNumericCellValue());
                                //objTabEx.setSigBeneficiario(beneficiarioEx);
                            } else {
                                mensaje(2, "Datos no cargados error en la fila " + (fila + 1) + " y columna " + (columna + 1) + " ");
                                mensaje(2, "Verificar el archivo y volver a cargar los datos");
                                return false;
                            }
                            break;
                        case 4://Raciones Mujeres (Int)
                            if (sheet.getRow(fila).getCell(columna).getNumericCellValue() != 0) {
                                beneficiarioEx.setMujeres((int) sheet.getRow(fila).getCell(columna).getNumericCellValue());
                            } else {
                                mensaje(2, "Datos no cargados error en la fila " + (fila + 1) + " y columna " + (columna + 1) + " ");
                                mensaje(2, "Verificar el archivo y volver a cargar los datos");
                                return false;
                            }
                            break;
                        case 5://Raciones PNC (Int)
                            if (sheet.getRow(fila).getCell(columna).getNumericCellValue() != 0) {
                                beneficiarioEx.setPnc((int) sheet.getRow(fila).getCell(columna).getNumericCellValue());
                            } else {
                                mensaje(2, "Datos no cargados error en la fila " + (fila + 1) + " y columna " + (columna + 1) + " ");
                                mensaje(2, "Verificar el archivo y volver a cargar los datos");
                                return false;
                            }
                            break;
                        case 6://Raciones Niños (Int)
                            if (sheet.getRow(fila).getCell(columna).getNumericCellValue() != 0) {
                                beneficiarioEx.setNiños((int) sheet.getRow(fila).getCell(columna).getNumericCellValue());
                            } else {
                                mensaje(2, "Datos no cargados error en la fila " + (fila + 1) + " y columna " + (columna + 1) + " ");
                                mensaje(2, "Verificar el archivo y volver a cargar los datos");
                                return false;
                            }
                            break;
                        case 7://Raciones Niñas (Int)
                            if (sheet.getRow(fila).getCell(columna).getNumericCellValue() != 0) {
                                beneficiarioEx.setNiñas((int) sheet.getRow(fila).getCell(columna).getNumericCellValue());
                            } else {
                                mensaje(2, "Datos no cargados error en la fila " + (fila + 1) + " y columna " + (columna + 1) + " ");
                                mensaje(2, "Verificar el archivo y volver a cargar los datos");
                                return false;
                            }
                            break;
                        case 8://Raciones Ancianos (Int)
                            if (sheet.getRow(fila).getCell(columna).getNumericCellValue() != 0) {
                                beneficiarioEx.setAncianos((int) sheet.getRow(fila).getCell(columna).getNumericCellValue());
                            } else {
                                mensaje(2, "Datos no cargados error en la fila " + (fila + 1) + " y columna " + (columna + 1) + " ");
                                mensaje(2, "Verificar el archivo y volver a cargar los datos");
                                return false;
                            }
                            break;
                        case 9://Raciones Ancianas (Int)
                            if (sheet.getRow(fila).getCell(columna).getNumericCellValue() != 0) {
                                beneficiarioEx.setAncianas((int) sheet.getRow(fila).getCell(columna).getNumericCellValue());
                            } else {
                                mensaje(2, "Datos no cargados error en la fila " + (fila + 1) + " y columna " + (columna + 1) + " ");
                                mensaje(2, "Verificar el archivo y volver a cargar los datos");
                                return false;
                            }

                            break;
                    }
                }
                objTabEx.setSigCentro(centro.buscarCentro(sessionbean.getUsu().getSigCentro().getPkIdCentro()));
                objTabEx.setTotal(beneficiarioEx.getPersonal() + beneficiarioEx.getHombres() + beneficiarioEx.getMujeres() + beneficiarioEx.getPnc() + beneficiarioEx.getNiños() + beneficiarioEx.getNiñas() + beneficiarioEx.getAncianos() + beneficiarioEx.getAncianas());
                objTabEx.setSigBeneficiario(beneficiarioEx);
                listTabEx.add(objTabEx);
            }
            return true;
        } catch (NumberFormatException e) {
            ERROR_SISTEMA("TabuladorBean.comprobarArchivoExcel", e);
            return false;
        } finally {
            sheet = null;
            fila = null;
            columna = null;
            objTabEx = null;
            archivo.borrarArchivo(urlArchivo);//BORRAR EL ARCHIVO
            archivo = null;
            urlArchivo = null;
        }

    }

    //Redirecciona a página de inicio
    public String inicio() {
        return "/secure/default.ues" + "?faces-redirect=true";
    }

    public List<SigTabuladorDTO> getTodosLosDesayunos() {
        return desayunoList = tabuladorBO.desayuno(sessionbean.getUsu().getSigCentro().getPkIdCentro(), sessionbean.getUsu().getSigRol().getPkRol(), sessionbean.getUsu().getCodigo());
    }

    public List<SigTabuladorDTO> getTodosLosAlmuerzos() {
        return almuerzoList = tabuladorBO.almuerzo(sessionbean.getUsu().getSigCentro().getPkIdCentro(), sessionbean.getUsu().getSigRol().getPkRol(), sessionbean.getUsu().getCodigo());
    }

    public List<SigTabuladorDTO> getTodasLosCenas() {
        return cenaList = tabuladorBO.cena(sessionbean.getUsu().getSigCentro().getPkIdCentro(), sessionbean.getUsu().getSigRol().getPkRol(), sessionbean.getUsu().getCodigo());
    }

    public List<SigTabuladorDTO> getTodosLosRefrigerios() {
        return refrigerioList = tabuladorBO.refrigerio(sessionbean.getUsu().getSigCentro().getPkIdCentro(), sessionbean.getUsu().getSigRol().getPkRol(), sessionbean.getUsu().getCodigo());
    }
    //Fin           
}
