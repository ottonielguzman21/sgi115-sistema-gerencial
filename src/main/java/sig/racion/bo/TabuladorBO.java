/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.racion.bo;

import sig.login.bean.SessionBean;
import sig.racion.dto.SigTabuladorDTO;
import sig.racion.entity.SigTabulador;
import java.util.List;

/**
 *
 * @author Elsy
 */
public interface TabuladorBO {
   public void guardarTabulador(SigTabulador tabulador, SessionBean usu);
   public void actualizarTabulador(SigTabulador tabulador, SessionBean usu);
   public List<SigTabulador> tabuladores();
   public SigTabulador buscarTabulador(Integer idTabulador);
   public List<SigTabuladorDTO> desayuno(int idCentro, int idRol, String codigo);
   public List<SigTabuladorDTO> almuerzo(int idCentro, int idRol, String codigo);
   public List<SigTabuladorDTO> cena(int idCentro, int idRol, String codigo);
   public List<SigTabuladorDTO> refrigerio(int idCentro, int idRol, String codigo);

    public boolean insertTabList(List<SigTabulador> listTabEx, SessionBean usu);
}
