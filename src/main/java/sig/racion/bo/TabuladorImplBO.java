/*Utilizado Alexis*/

 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.racion.bo;

import sig.login.bean.SessionBean;
import sig.login.dao.CrudDAO;
import sig.racion.dao.TabuladorDAO;
import sig.racion.dto.SigTabuladorDTO;
import sig.racion.entity.SigTabulador;
import java.util.List;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import static sig.login.clase.ConstantesSistema.ERROR_SISTEMA;

/**
 *
 * @author Elsy
 */
@Getter
@Service(value = "tabuladorBO")
public class TabuladorImplBO implements TabuladorBO {

    @Autowired
    private TabuladorDAO tabuladorDAO;
    @Autowired
    private CrudDAO crudDAO;

    @Override
    public void guardarTabulador(SigTabulador tabulador, SessionBean usu) {
        tabuladorDAO.guardarTabulador(tabulador);
        crudDAO.scriptSQL("insert into sig_bitacora (fecha, fk_usuario, fk_rol, fk_centro, tabla, accion, informacion) Values(NOW(),'" + usu.getUsu().getCodigo() + "', '" + usu.getUsu().getSigRol().getPkRol() + "', '" + usu.getUsu().getSigCentro().getPkIdCentro() + "','sig_tabulador','INSERT', '" + tabulador.toString() + "')");

    }

    @Override
    public void actualizarTabulador(SigTabulador tabulador, SessionBean usu) {
        tabuladorDAO.actualizarTabulador(tabulador);
        crudDAO.scriptSQL("insert into sig_bitacora (fecha, fk_usuario, fk_rol, fk_centro, tabla, accion, informacion) Values(NOW(),'" + usu.getUsu().getCodigo() + "', '" + usu.getUsu().getSigRol().getPkRol() + "', '" + usu.getUsu().getSigCentro().getPkIdCentro() + "','sig_tabulador','UPDATE', '" + tabulador.toString() + "')");
    }

    @Override
    public List<SigTabulador> tabuladores() {
        return tabuladorDAO.tabuladores();
    }

    @Override
    public SigTabulador buscarTabulador(Integer idTabulador) {
        return tabuladorDAO.buscarTabulador(idTabulador);
    }

    @Override
    public List<SigTabuladorDTO> desayuno(int idCentro, int idRol, String codigo) {
        return tabuladorDAO.desayuno(idCentro, idRol, codigo);
    }

    @Override
    public List<SigTabuladorDTO> almuerzo(int idCentro, int idRol, String codigo) {
        return tabuladorDAO.almuerzo(idCentro, idRol, codigo);
    }

    @Override
    public List<SigTabuladorDTO> cena(int idCentro, int idRol, String codigo) {
        return tabuladorDAO.cena(idCentro, idRol, codigo);
    }

    @Override
    public List<SigTabuladorDTO> refrigerio(int idCentro, int idRol, String codigo) {
        return tabuladorDAO.refrigerio(idCentro, idRol, codigo);
    }

    @Override
    public boolean insertTabList(List<SigTabulador> listTraEx, SessionBean usu) {
        try {
            for (SigTabulador obj : listTraEx) {
                tabuladorDAO.guardarTabulador(obj);
                crudDAO.scriptSQL("insert into sig_bitacora (fecha, fk_usuario, fk_rol, fk_centro, tabla, accion, informacion) Values(NOW(),'" + usu.getUsu().getCodigo() + "', '" + usu.getUsu().getSigRol().getPkRol() + "', '" + usu.getUsu().getSigCentro().getPkIdCentro() + "','sig_tabulador','Archivo', '" + obj.toString() + "')");

            }
            return true;
        } catch (Exception e) {
            ERROR_SISTEMA("TabuladorImplBO.insertDiagList", e);
            return false;
        }
    }

}
