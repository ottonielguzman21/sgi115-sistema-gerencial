/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.racion.entity;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Elsy
 */
@Setter
@Getter
public class SigMes {
    private int id;
    private String nombre;
    public SigMes(int id, String nombre){
        this.id=id;
        this.nombre=nombre;
    }

    public SigMes() {
         }
    
}
