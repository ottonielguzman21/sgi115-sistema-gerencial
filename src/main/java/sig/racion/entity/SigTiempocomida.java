/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.racion.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Elsy
 */
@Entity
@Table(name = "sig_tcomida",catalog="sig115")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SigTiempocomida implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "PK_TCOMIDA")
    private Integer idTcomida;
    @Column(name = "DESAYUNO")
    private Boolean desayuno;
    @Column(name = "ALMUERZO")
    private Boolean almuerzo;
    @Column(name = "CENA")
    private Boolean cena;
    @Column(name = "REFRIGERIO")
    private Boolean refrigerio;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sigTcomida")
    private List<SigTabulador> tabuladorList;

    @Override
    public String toString() {
        return "ID Tiempo comida= " + idTcomida + ", Desayuno= " + desayuno + ", Almuerzo= " + almuerzo + ", Cena= " + cena + ", Refrigerio= " + refrigerio + ", tabuladorList= " + tabuladorList + '.';
    }

    
    
}
