/*Utilizado Alexis*/

 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.racion.dao;

import sig.racion.dto.SigTabuladorDTO;
import sig.racion.entity.SigTabulador;
import java.util.List;
import lombok.Getter;
import org.bouncycastle.asn1.isismtt.x509.Restriction;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Elsy
 */
@Getter
@Transactional(value = "TMA")
@Service(value = "tabuladorDAO")
public class TabuladorImplDAO extends HibernateDaoSupport implements TabuladorDAO {

    @Autowired
    public TabuladorImplDAO(@Qualifier(value = "SFA") SessionFactory sf) {
        super.setSessionFactory(sf);
    }

    @Override
    public void guardarTabulador(SigTabulador tabulador) {
        currentSession().persist(tabulador);
    }

    @Override
    public void actualizarTabulador(SigTabulador tabulador) {
        currentSession().saveOrUpdate(tabulador);
    }

    @Override
    public List<SigTabulador> tabuladores() {
        return currentSession().createCriteria(SigTabulador.class).list();
    }

    @Override
    public SigTabulador buscarTabulador(Integer idTabulador) {
        return (SigTabulador) currentSession().createCriteria(SigTabulador.class)
                .add(Restrictions.eq("idTabulador", idTabulador)).uniqueResult();
    }

    @Override
    public List<SigTabuladorDTO> desayuno(int idCentro, int idRol, String codigo) {
        List<SigTabuladorDTO> listaDesayuno = null;
        List desayunoLista = currentSession().createSQLQuery("SELECT "
                + "sig_tabulador.FECHA AS fecha, "
                + "sig_beneficiario.personal AS personal, "
                + "sig_beneficiario.hombres AS hombres, "
                + "sig_beneficiario.mujeres AS mujeres, "
                + "sig_beneficiario.pnc AS pnc, "
                + "sig_beneficiario.niños AS niños, "
                + "sig_beneficiario.niñas AS niñas, "
                + "sig_beneficiario.ancianos AS ancianos, "
                + "sig_beneficiario.ancianas AS ancianas, "
                + "(sig_beneficiario.personal+sig_beneficiario.hombres+sig_beneficiario.mujeres+sig_beneficiario.pnc+sig_beneficiario.niños+sig_beneficiario.niñas+sig_beneficiario.ancianos+sig_beneficiario.ancianas "
                + "FROM "
                + "sig_tiempocomida sig_tiempocomida INNER JOIN sig_tabulador sig_tabulador ON sig_tiempocomida.ID_TCOMIDA = sig_tabulador.ID_TCOMIDA "
                + "INNER JOIN sig_beneficiario sig_beneficiario ON sig_tabulador.ID_beneficiario = sig_beneficiario.ID_beneficiario "
                + "INNER JOIN sig_centro sig_centro ON sig_tabulador.pk_idCentro = sig_centro.pk_idCentro "
                + "INNER JOIN sig_usuario sig_usuario ON sig_centro.pk_idCentro= sig_usuario.fk_centro "
                + "INNER JOIN sig_rol sig_rol ON sig_usuario.fk_rol= sig_rol.pk_rol "
                + "WHERE sig_centro.pk_idCentro=? "
                + "AND sig_tiempocomida.DESAYUNO=1 AND sig_rol.pk_rol=? AND sig_usuario.codigo= ?"
                + "GROUP BY FECHA").addScalar("fecha").addScalar("personal").addScalar("hombres").addScalar("mujeres").addScalar("pnc").addScalar("niños").addScalar("niñas").addScalar("ancianos").addScalar("ancianas").addScalar("total").setParameter(0, idCentro).setParameter(1, idRol).setParameter(2, codigo).setResultTransformer(Transformers.aliasToBean(SigTabuladorDTO.class)).list();
        listaDesayuno = (List<SigTabuladorDTO>) desayunoLista;
        return listaDesayuno;
    }

    @Override
    public List<SigTabuladorDTO> almuerzo(int idCentro, int idRol, String codigo) {
        List<SigTabuladorDTO> listaAlmuerzo = null;
        List almuerzoLista = currentSession().createSQLQuery("SELECT "
                + "sig_tabulador.FECHA AS fecha, "
                + "sig_beneficiario.personal AS personal, "
                + "sig_beneficiario.hombres AS hombres, "
                + "sig_beneficiario.mujeres AS mujeres, "
                + "sig_beneficiario.pnc AS pnc, "
                + "sig_beneficiario.niños AS niños, "
                + "sig_beneficiario.niñas AS niñas, "
                + "sig_beneficiario.ancianos AS ancianos, "
                + "sig_beneficiario.ancianas AS ancianas, "
                + "(sig_beneficiario.personal+sig_beneficiario.hombres+sig_beneficiario.mujeres+sig_beneficiario.pnc+sig_beneficiario.niños+sig_beneficiario.niñas+sig_beneficiario.ancianos+sig_beneficiario.ancianas "
                + "FROM "
                + "sig_tiempocomida sig_tiempocomida INNER JOIN sig_tabulador sig_tabulador ON sig_tiempocomida.ID_TCOMIDA = sig_tabulador.ID_TCOMIDA "
                + "INNER JOIN sig_beneficiario sig_beneficiario ON sig_tabulador.ID_beneficiario = sig_beneficiario.ID_beneficiario "
                + "INNER JOIN sig_centro sig_centro ON sig_tabulador.pk_idCentro = sig_centro.pk_idCentro "
                + "INNER JOIN sig_usuario sig_usuario ON sig_centro.pk_idCentro= sig_usuario.fk_centro "
                + "INNER JOIN sig_rol sig_rol ON sig_usuario.fk_rol= sig_rol.pk_rol "
                + "WHERE sig_centro.pk_idCentro=? "
                + "AND sig_tiempocomida.ALMUERZO=1 AND sig_rol.pk_rol=? AND sig_usuario.codigo= ?"
                + "GROUP BY FECHA").addScalar("fecha").addScalar("personal").addScalar("hombres").addScalar("mujeres").addScalar("pnc").addScalar("niños").addScalar("niñas").addScalar("ancianos").addScalar("ancianas").addScalar("total").setParameter(0, idCentro).setParameter(1, idRol).setParameter(2, codigo).setResultTransformer(Transformers.aliasToBean(SigTabuladorDTO.class)).list();
        listaAlmuerzo = (List<SigTabuladorDTO>) almuerzoLista;
        return listaAlmuerzo;
    }

    @Override
    public List<SigTabuladorDTO> cena(int idCentro, int idRol, String codigo) {
        List<SigTabuladorDTO> listaCena = null;
        List cenaLista = currentSession().createSQLQuery("SELECT "
                + "sig_tabulador.FECHA AS fecha, "
                + "sig_beneficiario.personal AS personal, "
                + "sig_beneficiario.hombres AS hombres, "
                + "sig_beneficiario.mujeres AS mujeres, "
                + "sig_beneficiario.pnc AS pnc, "
                + "sig_beneficiario.niños AS niños, "
                + "sig_beneficiario.niñas AS niñas, "
                + "sig_beneficiario.ancianos AS ancianos, "
                + "sig_beneficiario.ancianas AS ancianas, "
                + "(sig_beneficiario.personal+sig_beneficiario.hombres+sig_beneficiario.mujeres+sig_beneficiario.pnc+sig_beneficiario.niños+sig_beneficiario.niñas+sig_beneficiario.ancianos+sig_beneficiario.ancianas "
                + "FROM "
                + "sig_tiempocomida sig_tiempocomida INNER JOIN sig_tabulador sig_tabulador ON sig_tiempocomida.ID_TCOMIDA = sig_tabulador.ID_TCOMIDA "
                + "INNER JOIN sig_beneficiario sig_beneficiario ON sig_tabulador.ID_beneficiario = sig_beneficiario.ID_beneficiario "
                + "INNER JOIN sig_centro sig_centro ON sig_tabulador.pk_idCentro = sig_centro.pk_idCentro "
                + "INNER JOIN sig_usuario sig_usuario ON sig_centro.pk_idCentro= sig_usuario.fk_centro "
                + "INNER JOIN sig_rol sig_rol ON sig_usuario.fk_rol= sig_rol.pk_rol "
                + "WHERE sig_centro.pk_idCentro=? "
                + "AND sig_tiempocomida.CENA=1 AND sig_rol.pk_rol=? AND sig_usuario.codigo= ?"
                + "GROUP BY FECHA").addScalar("fecha").addScalar("personal").addScalar("hombres").addScalar("mujeres").addScalar("pnc").addScalar("niños").addScalar("niñas").addScalar("ancianos").addScalar("ancianas").addScalar("total").setParameter(0, idCentro).setParameter(1, idRol).setParameter(2, codigo).setResultTransformer(Transformers.aliasToBean(SigTabuladorDTO.class)).list();
        listaCena = (List<SigTabuladorDTO>) cenaLista;
        return listaCena;
    }

    @Override
    public List<SigTabuladorDTO> refrigerio(int idCentro, int idRol, String codigo) {
        List<SigTabuladorDTO> listaRefrigerio = null;
        List refrigerioLista = currentSession().createSQLQuery("SELECT "
                + "sig_tabulador.FECHA AS fecha, "
                + "sig_beneficiario.personal AS personal, "
                + "sig_beneficiario.hombres AS hombres, "
                + "sig_beneficiario.mujeres AS mujeres, "
                + "sig_beneficiario.pnc AS pnc, "
                + "sig_beneficiario.niños AS niños, "
                + "sig_beneficiario.niñas AS niñas, "
                + "sig_beneficiario.ancianos AS ancianos, "
                + "sig_beneficiario.ancianas AS ancianas, "
                + "(sig_beneficiario.personal+sig_beneficiario.hombres+sig_beneficiario.mujeres+sig_beneficiario.pnc+sig_beneficiario.niños+sig_beneficiario.niñas+sig_beneficiario.ancianos+sig_beneficiario.ancianas "
                + "FROM "
                + "sig_tiempocomida sig_tiempocomida INNER JOIN sig_tabulador sig_tabulador ON sig_tiempocomida.ID_TCOMIDA = sig_tabulador.ID_TCOMIDA "
                + "INNER JOIN sig_beneficiario sig_beneficiario ON sig_tabulador.ID_beneficiario = sig_beneficiario.ID_beneficiario "
                + "INNER JOIN sig_centro sig_centro ON sig_tabulador.pk_idCentro = sig_centro.pk_idCentro "
                + "INNER JOIN sig_usuario sig_usuario ON sig_centro.pk_idCentro= sig_usuario.fk_centro "
                + "INNER JOIN sig_rol sig_rol ON sig_usuario.fk_rol= sig_rol.pk_rol "
                + "WHERE sig_centro.pk_idCentro=? "
                + "AND sig_tiempocomida.REFRIGERIO=1 AND sig_rol.pk_rol=? AND sig_usuario.codigo= ?"
                + "GROUP BY FECHA").addScalar("fecha").addScalar("personal").addScalar("hombres").addScalar("mujeres").addScalar("pnc").addScalar("niños").addScalar("niñas").addScalar("ancianos").addScalar("ancianas").addScalar("total").setParameter(0, idCentro).setParameter(1, idRol).setParameter(2, codigo).setResultTransformer(Transformers.aliasToBean(SigTabuladorDTO.class)).list();
        listaRefrigerio = (List<SigTabuladorDTO>) refrigerioLista;
        return listaRefrigerio;
    }

}
