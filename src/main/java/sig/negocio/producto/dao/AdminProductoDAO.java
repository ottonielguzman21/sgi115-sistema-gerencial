/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.negocio.producto.dao;

import sig.dto.IngresoBodegaDto;
import sig.negocio.producto.entity.SigProducto;
import sig.negocio.producto.entity.SigRubro;
import sig.negocio.producto.entity.SigTipoproducto;
import sig.negocio.producto.entity.SigUnidadmedida;
import java.util.List;

/**
 *
 * @author Alexis Lopez
 */
public interface AdminProductoDAO {

    //PRODUCTO
    List<SigProducto> listProducto();

    List<SigProducto> listProdPere(int tipo);

    //TIPOPRODUCTO
    public List<SigTipoproducto> listTipo();

    public List<SigTipoproducto> listTipoAll();

    List<IngresoBodegaDto> listProCentroBodega(int centro);


    //Rubro
    public List<SigRubro> listRubro();
    public List<SigRubro> listRubroActivo();

    //UNIDADMEDIDA
    public List<SigUnidadmedida> listUnidad();

    public List<SigUnidadmedida> listUnidadTodos();

    public SigProducto encontrarProducto(String codProducto);

}
