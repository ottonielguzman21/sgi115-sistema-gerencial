package sig.negocio.producto.entity;
// Generated 08-18-2019 03:27:04 PM by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.Getter;
import lombok.Setter;

/**
 * SigUnidadmedida generated by hbm2java
 */
@Entity
@Table(name="sig_unidadmedida"
    ,catalog="sig115"
)
@Getter
@Setter
@ManagedBean
@ViewScoped
public class SigUnidadmedida  implements java.io.Serializable {


@Id
    @Column(name="pk_unidadmedida", unique=true, nullable=false)
     private int codigo;
     @Column(name="nombre", length=250)
     private String nombre;
     @OneToMany(fetch=FetchType.LAZY, mappedBy="sigUnidadmedida")
     private Set<SigProducto> sigProductos = new HashSet<SigProducto>(0);

    public SigUnidadmedida() {
    }

	
    public SigUnidadmedida(int codigo) {
        this.codigo = codigo;
    }
    public SigUnidadmedida(int codigo, String nombre, Set<SigProducto> sigProductos) {
       this.codigo = codigo;
       this.nombre = nombre;
       this.sigProductos = sigProductos;
    }
   
    public int getCodigo() {
        return this.codigo;
    }
    
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    
    public String getNombre() {
        return this.nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Set<SigProducto> getSigProductos() {
        return this.sigProductos;
    }
    
    public void setSigProductos(Set<SigProducto> sigProductos) {
        this.sigProductos = sigProductos;
    }

        @Transient
    private boolean unidadS;

}


