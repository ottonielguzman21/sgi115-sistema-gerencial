/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.negocio.producto.bean;

import sig.login.bean.SessionBean;
import sig.login.clase.Mensajes;
import sig.negocio.producto.bo.AdminProductoBO;
import sig.negocio.producto.entity.SigRubro;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.persistence.Transient;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Alexis Lopez
 */

@Getter
@Setter
@ManagedBean
@ViewScoped
public class RubroBean extends Mensajes  {
    @ManagedProperty(value = "#{adminProductoBO}")
    private AdminProductoBO adminProductoBO;

    private List<SigRubro> listRubro;
    private List<SigRubro> listRubroActivo;
    private SigRubro rubro;
    @ManagedProperty(value = "#{sessionBean}")
    private SessionBean sessionbean;

    public RubroBean() {
        rubro = new SigRubro();
    }

    @PostConstruct
    public void main() {
        rubro = new SigRubro();
        listRubro= adminProductoBO.listRubro();
        listRubroActivo= adminProductoBO.listRubroActivo();
    }
    public void insertRubro() {
            if (adminProductoBO.insertRubro(this,sessionbean)) {
                mensaje(1, "Rubro agregado correctamente");
                rubro = new SigRubro();
                listRubro = adminProductoBO.listRubro();
            } else {
                mensaje(2, "Error al Agregar Nuevo Rubro");
            }
    }
    

    public void updateEstadoRubro(SigRubro ru) {
        ru.setEstado(!ru.isEstado());
        if (adminProductoBO.updateRubro(ru,sessionbean)) {
            listRubro = adminProductoBO.listRubro();
            mensaje(1, "Estado del producto modificado correctamente");
        } else {
            mensaje(2, "Error al modificar el estado");
        }
    }
    
    public void onRowEdit(SigRubro ru) {
        if (adminProductoBO.updateRubro(ru,sessionbean)) {
            mensaje(1, "Rubro modificado correctamente");
        } else {
            mensaje(2, "Error al modificar el Rubro");
        }
    }

    public void onRowCancel() {
        mensaje(2, "Modificación cancelada");
    }
    
    public void rubroSeleccionado(SigRubro rb) {
        if (listRubro.size() > 0) {
            for(SigRubro ru:listRubro){
                ru.setRubroS(false);
                if(ru.getIdRubro() == rb.getIdRubro()){
                    ru.setRubroS(true);
                }
            }
        }
    }
   
}
