/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.negocio.producto.bean;

import sig.negocio.producto.entity.SigUnidadmedida;
import sig.negocio.producto.bo.AdminProductoBO;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Alexis Lopez
 */
@Getter
@Setter
@ManagedBean
@ViewScoped
public class UnidadMedidaBean {
    @ManagedProperty(value = "#{adminProductoBO}")
    private AdminProductoBO adminProductoBO;

    private List<SigUnidadmedida> listUnidad;
    private SigUnidadmedida unidad;

    public UnidadMedidaBean() {
        unidad = new SigUnidadmedida();
    }

    @PostConstruct
    public void main() {
        unidad = new SigUnidadmedida();
        listUnidad = adminProductoBO.listUnidad();
    }
    
    public void tipoSeleccionado(SigUnidadmedida um) {
        if (listUnidad.size() > 0) {
            for(SigUnidadmedida ud:listUnidad){
                ud.setUnidadS(false);
                if(ud.getCodigo() == um.getCodigo()){
                    ud.setUnidadS(true);
                }
            }
        }
    }
}