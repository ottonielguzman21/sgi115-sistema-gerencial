/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.negocio.producto.bo;

import sig.dto.IngresoBodegaDto;
import sig.login.bean.SessionBean;
import java.util.List;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sig.negocio.producto.dao.AdminProductoDAO;
import sig.login.dao.CrudDAO;
import sig.negocio.producto.entity.SigProducto;
import sig.negocio.producto.entity.SigTipoproducto;
import sig.negocio.producto.entity.SigUnidadmedida;
import sig.negocio.producto.entity.SigRubro;
import sig.negocio.producto.bean.ProductoBean;
import sig.negocio.producto.bean.RubroBean;
import javax.faces.bean.ManagedProperty;


/**
 *
 * @author Alexis Lopez
 */

@Getter
@Service(value = "adminProductoBO")
public class AdminProductoImplBO implements AdminProductoBO {
    @ManagedProperty(value = "#{sessionBean}")
    private SessionBean sessionbean;
    @Autowired
    private CrudDAO crudDAO;
    @Autowired
    private AdminProductoDAO adminProductoDAO;
    
    @Override
    public List<SigProducto> listProducto() {
        return adminProductoDAO.listProducto();
    }
    
    @Override
    public List<IngresoBodegaDto> listProCentroBodega(int centro) {
        return adminProductoDAO.listProCentroBodega(centro);
    }
    
    @Override
    public List<SigProducto> listProdPere(int tipo) {
        return adminProductoDAO.listProdPere(tipo);
    }

    @Override
    public boolean insertProducto(ProductoBean bean, SessionBean usu) {
        try {
            bean.getPro().setEstado(true);
            bean.getPro().setCodigo(bean.getPro().getRubroS()+String.valueOf(crudDAO.maxRubro("SELECT LPAD(COUNT(pk_producto)+1,12,0) FROM sig_producto WHERE pk_producto LIKE '"+bean.getPro().getRubroS()+"%'"))); 
            bean.getPro().setSigRubro(new SigRubro(bean.getPro().getRubroS()));
            crudDAO.insertObj(bean.getPro());
            crudDAO.scriptSQL("insert into sig_bitacora (fecha, fk_usuario, fk_rol, fk_centro, tabla, accion, informacion) Values(NOW(),'"+usu.getUsu().getCodigo()+"', '"+usu.getUsu().getSigRol().getPkRol()+"', '"+usu.getUsu().getSigCentro().getPkIdCentro()+"','sig_producto','INSERT', '"+ bean.getPro().toString()+"')");
            return true;
        } catch (Exception e) {
            System.out.println("** AdminProductoImplBO ** insert producto ** " + e);
            return false;
        }
    }

    @Override
    public boolean updateProducto(SigProducto pro, SessionBean usu) {
        try {
            crudDAO.updateObj(pro);
             crudDAO.scriptSQL("insert into sig_bitacora (fecha, fk_usuario, fk_rol, fk_centro, tabla, accion, informacion) Values(NOW(),'"+usu.getUsu().getCodigo()+"', '"+usu.getUsu().getSigRol().getPkRol()+"', "+usu.getUsu().getSigCentro().getPkIdCentro()+",'sig_producto','UPDATE', '"+ pro.toString()+"')");
            return true;
        } catch (Exception e) {
            System.out.println("** AdminProductoImplBO ** update Producto ** " + e);
            return false;
        }
    }
    

    @Override
    public List<SigTipoproducto> listTipo() {
       return adminProductoDAO.listTipo();
    }

    @Override
    public List<SigTipoproducto> listTipoTodos() {
       return adminProductoDAO.listTipoAll();
    }

    @Override
    public List<SigUnidadmedida> listUnidad() {
        return adminProductoDAO.listUnidad();
    }

    @Override
    public List<SigUnidadmedida> listUnidadTodos() {
       return adminProductoDAO.listUnidadTodos();
    }

    @Override
    public List<SigRubro> listRubro() {
        return adminProductoDAO.listRubro();
    }
    
    @Override
    public List<SigRubro> listRubroActivo() {
        return adminProductoDAO.listRubroActivo();
    }
    
    @Override
    public boolean insertRubro(RubroBean bean, SessionBean usu) {
        try {
            bean.getRubro().setEstado(true);
            crudDAO.insertObj(bean.getRubro());
            crudDAO.scriptSQL("insert into sig_bitacora (fecha, fk_usuario, fk_rol, fk_centro, tabla, accion, informacion) Values(NOW(),'"+usu.getUsu().getCodigo()+"', '"+usu.getUsu().getSigRol().getPkRol()+"', '"+usu.getUsu().getSigCentro().getPkIdCentro()+"','sig_rubro','INSERT', '"+ bean.getRubro().toString()+"')");
            return true;
        } catch (Exception e) {
            System.out.println("** AdminProductoImplBO ** insert rubro ** " + e);
            return false;
        }
    }

    @Override
    public boolean updateRubro(SigRubro rubro, SessionBean usu) {
        try {
            crudDAO.updateObj(rubro);
            crudDAO.scriptSQL("insert into sig_bitacora (fecha, fk_usuario, fk_rol, fk_centro, tabla, accion, informacion) Values(NOW(),'"+usu.getUsu().getCodigo()+"', '"+usu.getUsu().getSigRol().getPkRol()+"', '"+usu.getUsu().getSigCentro().getPkIdCentro()+"','sig_rubro','UPDATE', '"+ rubro.toString()+"')");
            return true;
        } catch (Exception e) {
            System.out.println("** AdminProductoImplBO ** insert Rubro ** " + e);
            return false;
        }
    }

    @Override
    public SigProducto encontrarProducto(String codProducto) {
        return adminProductoDAO.encontrarProducto(codProducto);
    }

}
