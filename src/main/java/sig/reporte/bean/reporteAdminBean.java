/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.reporte.bean;

import sig.login.bo.AdminUsuarioBO;
import sig.login.entity.SigCentro;
import sig.login.entity.SigRol;
import sig.login.entity.SigUsuario;
import sig.negocio.producto.bo.AdminProductoBO;
import sig.negocio.producto.entity.SigProducto;
import sig.negocio.producto.entity.SigRubro;
import sig.racion.entity.SigMes;
import sig.racion.entity.SigTabulador;
import sig.racion.entity.SigTiempocomida;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import lombok.Getter;
import lombok.Setter;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
/**
 *
 * @author Elsy
 */
@Getter
@Setter
@ManagedBean
@ViewScoped
public class reporteAdminBean {

    @ManagedProperty(value = "#{adminUsuarioBO}")
    private AdminUsuarioBO adminUsuarioBO;

    @ManagedProperty(value = "#{adminProductoBO}")
    private AdminProductoBO adminProductoBO;

    private List<SigUsuario> listUsu;
    private SigUsuario usu;
    private SigTiempocomida tcomida;
    private SigTabulador tab;
    private SigRubro rubro;
    private SigProducto pro;
    private List<SigRol> listRol;
    private List<SigCentro> listCentro;
    private int id_tcomida;
    private int idCentro;
    private int idRol;
    private int idRubro;
    private int idMes;
    private Date fechaInicio;
    private Date fechaFin;
    private List<SigRubro> listRubro;
    public List<SigMes> meses;
    private SigMes mesSeleccionado;
    private String image;
    private boolean desayuno;
    private boolean cena;
    private boolean almuerzo;
    private boolean refrigerio;
    private Date inicio;
    private SigCentro centroSeleccionado;
    public reporteAdminBean() {
    }

    @PostConstruct
    public void main() {
        usu = new SigUsuario();
        pro = new SigProducto();
        mesSeleccionado= new SigMes();
        listUsu = adminUsuarioBO.listUsuario();
        listRol = adminUsuarioBO.listRol();
        listCentro = adminUsuarioBO.listCentro();
        listRubro = adminProductoBO.listRubro();
        centroSeleccionado= new SigCentro();     
    }
    
    
    //PARA REPORTE DE RACIONES CON PARAMETROS
    
    public void centroSeleccionado() {
        idCentro = usu.getSigCentro().getPkIdCentro();
    }
    public void centroElegido() {
        idCentro = centroSeleccionado.getPkIdCentro();
    }

    public void rubroSeleccionado() {
        idRubro = pro.getSigRubro().getIdRubro();
    }
    
   public void inicioSeleccionado() {
       setFechaInicio(fechaInicio);
       System.out.println("Fecha inicio:" + fechaInicio);
    }
    
   public void finSeleccionado() {
        setFechaFin(fechaFin);
        System.out.println("Fecha fin:" + fechaFin);
    }


//REPORTE DE TABULADOR DE RACIONES SERVIDAS EN CENTROS DE ATENCION
   
//CENTROS DE RESGUARDO
    public void verReporteRaciones(int idCentro, Date fechaInicio, Date fechaFin) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();
        String ruta = servletContext.getRealPath("/reportes/racionResguardo.jasper");
        getReporteRaciones(ruta, idCentro, fechaInicio, fechaFin);
        FacesContext.getCurrentInstance().responseComplete();
    }

    public void getReporteRaciones(String ruta, int idCentro, Date fechaInicio, Date fechaFin) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        Connection conexion;
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/sig115", "root", "root");
        FacesContext facesContext = FacesContext.getCurrentInstance(); 
        ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();
        
        //Se definen los parametros si es que el reporte necesita
        HashMap<String, Object> parameter = new HashMap<String, Object>();

        try {
            File file = new File(ruta);

            HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();

            httpServletResponse.setContentType("application/pdf");
            httpServletResponse.addHeader("Content-Type", "application/pdf");

            JasperReport jasperReport = (JasperReport) JRLoader.loadObjectFromFile(file.getPath());
                                 
                parameter.put("nomCentro", usu.getSigCentro().getNomCentro());
                parameter.put("idCentro", idCentro);
                parameter.put("fechaInicio", fechaInicio);
                parameter.put("fechaFin", fechaFin);
                
              //  parameter.put("image", servletContext.getRealPath("/reportes/logo_bueno.jpg"));

            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameter, conexion);

            JRExporter jrExporter = null;
            jrExporter = new JRPdfExporter();
            jrExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            jrExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, httpServletResponse.getOutputStream());

            if (jrExporter != null) {
                try {
                    jrExporter.exportReport();
                } catch (JRException e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conexion != null) {
                try {
                    conexion.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

//REPORTE PARA EXISTENCIAS EN BODEGA
    
     public void verReporteExisBodega(int idCentro) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {

        FacesContext facesContext = FacesContext.getCurrentInstance();
        ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();
        String ruta = servletContext.getRealPath("/reportes/reporteExistenciasyConsumosBodega.jasper");
        getReporteExiBodega(ruta, idCentro);
        FacesContext.getCurrentInstance().responseComplete();
    }


    public void getReporteExiBodega(String ruta, int idCentro) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        Connection conexion;
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/sig115", "root", "root");
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();

        //Se definen los parametros si es que el reporte necesita
        HashMap<String, Object> parameter = new HashMap<String, Object>();

        try {
            File file = new File(ruta);

            HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();

            httpServletResponse.setContentType("application/pdf");
            httpServletResponse.addHeader("Content-Type", "application/pdf");

            JasperReport jasperReport = (JasperReport) JRLoader.loadObjectFromFile(file.getPath());
            parameter.put("idCentro", idCentro);
            //parameter.put("image", servletContext.getRealPath("/reportes/logo_bueno.jpg"));
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameter, conexion);

            JRExporter jrExporter = null;
            jrExporter = new JRPdfExporter();
            jrExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            jrExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, httpServletResponse.getOutputStream());

            if (jrExporter != null) {
                try {
                    jrExporter.exportReport();
                } catch (JRException e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conexion != null) {
                try {
                    conexion.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    //REPORTE CATALOGO DE PRODUCTOS
    public void verReporteCatalogoProducto(int idRubro, int idCentro) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {

        FacesContext facesContext = FacesContext.getCurrentInstance();
        ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();
        String ruta = servletContext.getRealPath("/reportes/reporteCatalogoProductos.jasper");
        getReporteCatalogoProducto(ruta, idRubro, idCentro);
        FacesContext.getCurrentInstance().responseComplete();
    }


    public void getReporteCatalogoProducto(String ruta, int idRubro, int idCentro) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        Connection conexion;
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/sig115", "root", "root");
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();
        

        //Se definen los parametros si es que el reporte necesita
        HashMap<String, Object> parameter = new HashMap<String, Object>();

        try {
            File file = new File(ruta);

            HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();

            httpServletResponse.setContentType("application/pdf");
            httpServletResponse.addHeader("Content-Type", "application/pdf");

            JasperReport jasperReport = (JasperReport) JRLoader.loadObjectFromFile(file.getPath());
            
            parameter.put("idRubro", idRubro);
            parameter.put("idCentro", idCentro);
            //parameter.put("image", servletContext.getRealPath("/reportes/logo_bueno.jpg"));
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameter, conexion);            

            JRExporter jrExporter = null;
            jrExporter = new JRPdfExporter();
            jrExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            jrExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, httpServletResponse.getOutputStream());

            if (jrExporter != null) {
                try {
                    jrExporter.exportReport();
                } catch (JRException e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conexion != null) {
                try {
                    conexion.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    //PARA REPORTE DE USUARIOS CON PARAMETROS
    public void rolSeleccionado() {
        idRol = usu.getSigRol().getPkRol();
    }

    public void verReporteUsuario(int idRol, int idCentro) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {

        FacesContext facesContext = FacesContext.getCurrentInstance();
        ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();
        String ruta = servletContext.getRealPath("/reportes/reporteUsuarios.jasper");
        getReporteUsuario(ruta, idRol, idCentro);
        FacesContext.getCurrentInstance().responseComplete();
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public void getReporteUsuario(String ruta, int idRol, int idCentro) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        Connection conexion;
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/sig115", "root", "root");
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ServletContext servletContext = (ServletContext) facesContext.getExternalContext().getContext();

        //Se definen los parametros si es que el reporte necesita
        HashMap<String, Object> parameter = new HashMap<String, Object>();

        try {
            File file = new File(ruta);

            HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();

            httpServletResponse.setContentType("application/pdf");
            httpServletResponse.addHeader("Content-Type", "application/pdf");

            JasperReport jasperReport = (JasperReport) JRLoader.loadObjectFromFile(file.getPath());
            parameter.put("idRol", idRol);
            parameter.put("idCentro", idCentro);
           // parameter.put("image", servletContext.getRealPath("/reportes/logo_bueno.jpg"));
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameter, conexion);

            JRExporter jrExporter = null;
            jrExporter = new JRPdfExporter();
            jrExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            jrExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, httpServletResponse.getOutputStream());

            if (jrExporter != null) {
                try {
                    jrExporter.exportReport();
                } catch (JRException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conexion != null) {
                try {
                    conexion.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
