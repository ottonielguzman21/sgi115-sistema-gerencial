/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.reporte.bean;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Alexis Lopez
 */

public class ReporteBitacora {
    
    public void getReporte(String ruta, String usuario, int centro, String rol) throws  ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
        Connection conexion;
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/sig115", "root", "root");

        //Se definen los parametros si es que el reporte necesita
        Map parameter = new HashMap();
        String cen;
        if (centro==0) cen="";
        else cen=centro+"";
        if (usuario==null) usuario="";
        if (rol==null) rol="";
        String sql=" WHERE fk_usuario='"+usuario+"' || fk_centro='"+cen+"' || fk_rol='"+rol+"'";
        
        if(!usuario.isEmpty() && !cen.isEmpty() && rol.isEmpty()){
            sql=" WHERE fk_usuario='"+usuario+"' && fk_centro='"+cen+"'";
        }
        if(!usuario.isEmpty() && !rol.isEmpty() && cen.isEmpty()){
            sql=" WHERE fk_usuario='"+usuario+"' && fk_rol='"+rol+"'";
        }
        
        if(usuario.isEmpty() && !rol.isEmpty() && !cen.isEmpty()){
            sql=" WHERE fk_centro='"+centro+"' && fk_rol='"+rol+"'";
        }
        
        if(!usuario.isEmpty() && !rol.isEmpty() && !cen.isEmpty()){
            sql=" WHERE fk_usuario='"+usuario+"' && fk_rol='"+rol+"' && fk_centro='"+centro+"'";
        }
    
        if(usuario.isEmpty() && cen.isEmpty() && rol.isEmpty()){
            parameter.put("sql_consulta"," ");
            
        }
        else{
            parameter.put("sql_consulta", sql);
        }
         

        try {
            File file = new File(ruta);

            HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();

            httpServletResponse.setContentType("application/pdf");
            httpServletResponse.addHeader("Content-Type", "application/pdf");

            JasperReport jasperReport = (JasperReport) JRLoader.loadObjectFromFile(file.getPath());

            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameter, conexion);

            JRExporter jrExporter = null;                      
            jrExporter = new JRPdfExporter();
            jrExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            jrExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, httpServletResponse.getOutputStream());

            if (jrExporter != null) {
                try {
                    jrExporter.exportReport();
                } catch (JRException e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (conexion != null) {
                try {
                    conexion.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

