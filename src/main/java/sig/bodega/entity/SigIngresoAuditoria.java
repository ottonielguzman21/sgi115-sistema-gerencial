package sig.bodega.entity;

import sig.login.entity.SigCatalogoMarca;
import sig.negocio.producto.entity.SigProducto;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Proxy;

/**
 *
 * @author Otoniel
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

@Entity
@Table(name = "sig_ingresob_aud", catalog = "sig115")
public class SigIngresoAuditoria implements Serializable{
    public SigIngresoAuditoria(SigProducto fk_codigo) {
        this.fk_codigo = fk_codigo;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "pk_IngresoB_aud", unique = true, nullable = false)//ok
    private int pk_IngresoBodega_aud;
    
    @Column(name = "precio_unit_aud", nullable = false, precision = 2 )//ok
    private Double precio_unit_aud;
    
    @Column(name = "cantidad_aud", nullable = false )//ok
    private int cantidad_aud;
    
    @Column(name = "num_factura_aud", nullable = false )//ok
    private int num_factura_aud;
    
    @Column(name = "realizado_por_aud", nullable = false, length = 50)//ok
    private String realizado_por_aud;
    
    @Column(name = "tipo_ingreso_aud", nullable = false, length = 20)//ok
    private String tipo_ingreso_aud;
    
    @Column(name = "nombre_distribuidor_aud", nullable = false, length = 50)//ok
    private String nombre_distribuidor_aud;
    
    @Column(name = "descripcion_aud", nullable = false, length = 50)//ok
    private String descripcion_aud;

    @Column(name = "fecha_ingreso_aud")//ok
    private Date fecha_ingreso_aud;
    
    @Column(name = "fecha_caducidad_aud")//ok
    private Date fecha_caducidad_aud;
    
    @Column(name = "fk_centro", nullable = false)
    private int fk_idCentro;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="fk_producto", nullable=false)
    private SigProducto fk_codigo;
    
    
    @Column(name = "fecha_registro_aud", nullable=false, length=30)//ok
    private Date fecha_registro_aud;
     
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="fk_marca", nullable=false)
    private SigCatalogoMarca fk_marca;
    
}
