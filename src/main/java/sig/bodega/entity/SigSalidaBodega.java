/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.bodega.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Gonzalez
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

@Entity
@Table(name = "sig_salida_bodega", catalog = "sig115")
public class SigSalidaBodega implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "pk_Salida_Bodega", unique = true, nullable = false)
    private int pk_SalidaBodega;
    
    @Column(name = "fk_producto", nullable = false, length = 19)
    private String fk_codigo;
    
    @Column(name = "fk_centro", nullable = false)
    private int fk_idCentro;
    
    @Column(name = "cantidad", nullable = false )
    private int cantidad;
    
    @Column(name = "fecha_salida", nullable = false)
    private Date fecha_salida;
    
    @Column(name = "realizado_por", nullable = false, length = 50)
    private String realizado_por;
    
    @Column(name = "entregado_a", nullable = false, length = 50)
    private String entregado_a;
    
    @Column(name = "observacion", nullable = true, length = 50)
    private String observacion;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="fk_Existencia_Bodega", nullable=false)
    private SigExistenciasBodega fk_ExistenciaBodega;
    
    @Column(name = "fecha_auditoria", nullable = false, length = 30)
    private String fecha_auditoria;
    
    @Override
    public String toString(){
        return "Producto: "+fk_codigo+", Cantidad= "+cantidad+", Fecha salida: "+fecha_salida+", Entregado a: "+entregado_a+", Justificacion= "+observacion+".";
    }
}
