package sig.bodega.entity;

import sig.login.entity.SigCatalogoMarca;
import sig.negocio.producto.entity.SigProducto;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Proxy;

/**
 *
 * @author abdiel
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

@Entity
@Table(name = "sig_existencia_bodega", catalog = "sig115")
public class SigExistenciasBodega implements Serializable{

    public SigExistenciasBodega(SigProducto fk_codigo) {
        this.fk_codigo = fk_codigo;
    }

    public SigExistenciasBodega(int pk_ExistenciaBodega) {
        this.pk_ExistenciaBodega = pk_ExistenciaBodega;
    }

    public SigExistenciasBodega(int pk_ExistenciaBodega, Double precio_unit, int cantidad, int num_factura, String realizado_por, String tipo_ingreso, String nombre_distribuidor, String descripcion, Date fecha_ingreso, Date fecha_caducidad, int fk_idCentro, SigProducto fk_codigo, String fecha_auditoria, SigCatalogoMarca fk_marca) {
        this.pk_ExistenciaBodega = pk_ExistenciaBodega;
        this.precio_unit = precio_unit;
        this.cantidad = cantidad;
        this.num_factura = num_factura;
        this.realizado_por = realizado_por;
        this.tipo_ingreso = tipo_ingreso;
        this.nombre_distribuidor = nombre_distribuidor;
        this.descripcion = descripcion;
        this.fecha_ingreso = fecha_ingreso;
        this.fecha_caducidad = fecha_caducidad;
        this.fk_idCentro = fk_idCentro;
        this.fk_codigo = fk_codigo;
        this.fecha_auditoria = fecha_auditoria;
        this.fk_marca = fk_marca;
    }
   
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "pk_existencia_bodega", unique = true, nullable = false)
    private int pk_ExistenciaBodega;
    
    @Column(name = "precio_unit", nullable = false, length = 8, precision = 2 )
    private Double precio_unit;
    
    @Column(name = "cantidad", nullable = false )
    private int cantidad;
    
    @Column(name = "num_factura", nullable = false )
    private int num_factura;
    
    @Column(name = "realizado_por", nullable = false, length = 50)
    private String realizado_por;
    
    @Column(name = "tipo_ingreso", nullable = false, length = 20)
    private String tipo_ingreso;
    
    @Column(name = "nombre_distribuidor", nullable = false, length = 50)
    private String nombre_distribuidor;
    
    @Column(name = "descripcion", nullable = false, length = 50)
    private String descripcion;

    @Column(name = "fecha_ingreso", length = 10)
    private Date fecha_ingreso;
    
    @Column(name = "fecha_caducidad", length = 10)
    private Date fecha_caducidad;
    
    @Column(name = "fk_centro", nullable = false)
    private int fk_idCentro;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="fk_producto", nullable=false)
    private SigProducto fk_codigo;
    
    @Column(name = "fecha_auditoria", nullable = false, length = 30)
    private String fecha_auditoria;
     
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="fk_marca", nullable=false)
    private SigCatalogoMarca fk_marca;

    @Override
    public String toString() {
        return  "Producto=" + descripcion+ ", " + fk_marca+", Precio unitario=" + precio_unit + ", Cantidad=" + cantidad + ", Num factura=" + num_factura + ", Realizado por=" + realizado_por + ", Tipo ingreso=" + tipo_ingreso + ", Nombre Distribuidor=" + nombre_distribuidor + ", Fecha ingreso=" + fecha_ingreso + ", Fecha caducidad=" + fecha_caducidad + '.';
    }
    
   @Transient
   private double subTotal=0.00;
}
