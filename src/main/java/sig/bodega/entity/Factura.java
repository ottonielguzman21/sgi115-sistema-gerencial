/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.bodega.entity;

import sig.login.entity.SigCatalogoMarca;
import java.util.Date;

/**
 *
 * @author otoniel21
 */
public class Factura {

    private int num_factura;
    private String realizado_por;
    private Date fecha_ingreso;
    private int fk_idCentro;
    private String fecha_auditoria;
    private SigCatalogoMarca fk_marca;
    private String nombre_distribuidor;//proveedor 
    private String tipo_ingreso;

    public Factura() {
    }
    
    public Factura(int num_factura, String realizado_por, Date fecha_ingreso, int fk_idCentro, String fecha_auditoria, SigCatalogoMarca fk_marca, String nombre_distribuidor, String tipo_ingreso) {
        this.num_factura = num_factura;
        this.realizado_por = realizado_por;
        this.fecha_ingreso = fecha_ingreso;
        this.fk_idCentro = fk_idCentro;
        this.fecha_auditoria = fecha_auditoria;
        this.fk_marca = fk_marca;
        this.nombre_distribuidor = nombre_distribuidor;
        this.tipo_ingreso = tipo_ingreso;
    }

    public Date getFecha_ingreso() {
        return fecha_ingreso;
    }

    public void setFecha_ingreso(Date fecha_ingreso) {
        this.fecha_ingreso = fecha_ingreso;
    }
    

    public int getNum_factura() {
        return num_factura;
    }

    public void setNum_factura(int num_factura) {
        this.num_factura = num_factura;
    }

    public String getRealizado_por() {
        return realizado_por;
    }

    public void setRealizado_por(String realizado_por) {
        this.realizado_por = realizado_por;
    }

    public int getFk_idCentro() {
        return fk_idCentro;
    }

    public void setFk_idCentro(int fk_idCentro) {
        this.fk_idCentro = fk_idCentro;
    }

    public String getFecha_auditoria() {
        return fecha_auditoria;
    }

    public void setFecha_auditoria(String fecha_auditoria) {
        this.fecha_auditoria = fecha_auditoria;
    }

    public SigCatalogoMarca getFk_marca() {
        return fk_marca;
    }

    public void setFk_marca(SigCatalogoMarca fk_marca) {
        this.fk_marca = fk_marca;
    }

    public String getNombre_distribuidor() {
        return nombre_distribuidor;
    }

    public void setNombre_distribuidor(String nombre_distribuidor) {
        this.nombre_distribuidor = nombre_distribuidor;
    }

    public String getTipo_ingreso() {
        return tipo_ingreso;
    }

    public void setTipo_ingreso(String tipo_ingreso) {
        this.tipo_ingreso = tipo_ingreso;
    }
    
    
}
