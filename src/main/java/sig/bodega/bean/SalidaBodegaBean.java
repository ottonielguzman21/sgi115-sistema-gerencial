package sig.bodega.bean;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import sig.bodega.bo.ExistenciaBodegaBO;
import sig.dto.SalidaBodegaDto;
import sig.bodega.bo.SalidaBodegaBO;
import sig.bodega.entity.Factura;
import sig.login.bean.SessionBean;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import lombok.Getter;
import lombok.Setter;
import sig.login.clase.Mensajes;
import sig.login.entity.SigCatalogoMarca;
import sig.bodega.entity.SigExistenciasBodega;
import sig.bodega.entity.SigSalidaBodega;
import sig.negocio.producto.bo.AdminProductoBO;
import sig.negocio.producto.entity.SigProducto;
import java.util.ArrayList;
import java.util.Date;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.omnifaces.util.Ajax;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;


/**
 *
 * @author Mario
 */
@ManagedBean(name="salidaBodegaBean")
@ViewScoped
@Getter
@Setter
public class SalidaBodegaBean extends Mensajes{
    
    @ManagedProperty(value = "#{salidaBodegaBO}")
    private SalidaBodegaBO salidaBO;
    
    @ManagedProperty(value = "#{sessionBean}")
    private SessionBean sessionbean;
    
    @ManagedProperty(value = "#{adminProductoBO}")
    private AdminProductoBO adminProductoBO;
    
    @ManagedProperty(value = "#{existenciaBodegaBO}")
    private ExistenciaBodegaBO ingresoBO;
    
    List<SalidaBodegaDto> existencias;
    
    private List<SigProducto> productos;
    public List<SigExistenciasBodega> exisBodega;
    public List<SigExistenciasBodega> exisBodegaSelec;
    public Factura factura;
    public Date fecha_salida;
    private List<SigSalidaBodega> listSalidaBodega;//para el listener siguiente
    private List<SigSalidaBodega> listActualSalida;//para el listener siguiente
    
    private String entregado_a;
    
    
    @PostConstruct
    public void main() {
    factura = new Factura();
    factura.setFecha_ingreso(new Date());
    factura.setFk_idCentro(sessionbean.getUsu().getSigCentro().getPkIdCentro());
    factura.setRealizado_por(sessionbean.getUsu().getNombre() + " " + sessionbean.getUsu().getApellido());
    existencias=salidaBO.listProductoCentro(factura.getFk_idCentro());
    exisBodega = salidaBO.connsultaPEPS(factura.getFk_idCentro());
    //exisBodegaSelec = salidaBO.connsultaPEPS(sessionbean.getUsu().getSigCentro().getPkIdCentro());
    productos = adminProductoBO.listProducto();
    listActualSalida = salidaBO.listSalidaBodega(factura.getFk_idCentro());
    listSalidaBodega = new ArrayList<>();
    } 
    public void validarForm(){
        if (entregado_a != "" && fecha_salida.toString()!=null && exisBodegaSelec.isEmpty()!=true) {
            Ajax.update("formSal");
            RequestContext.getCurrentInstance().execute(" PF('salidaProductos').hide(); PF('detalleSalPro').show();");
            salidasUpdate();
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERROR, POR FAVOR SELECCIONE AL MENOS UN PRODUCTO DE LA LISTA!","ss"));
        }
    }
    public List<SigSalidaBodega> salidasUpdate(){
        listSalidaBodega.clear();
        for(SigExistenciasBodega item : exisBodegaSelec){
            SigSalidaBodega salidaBodega = new SigSalidaBodega();
            salidaBodega.setFk_ExistenciaBodega(item);
            salidaBodega.setFk_codigo(item.getFk_codigo().getCodigo());            
            salidaBodega.setCantidad(item.getCantidad());
            salidaBodega.setRealizado_por(factura.getRealizado_por());//ok
            salidaBodega.setEntregado_a(entregado_a);
            salidaBodega.setObservacion("SIN OBSERVACION");
            salidaBodega.setFecha_auditoria(java.util.Calendar.getInstance().getTime().toString());
            salidaBodega.setFecha_salida(factura.getFecha_ingreso());
            salidaBodega.setFk_idCentro(factura.getFk_idCentro());
            listSalidaBodega.add(salidaBodega);
        }
        return listSalidaBodega;
    }
    
    public void asignarFecha(){
        factura.setFecha_ingreso(fecha_salida);
        System.out.println("en asignarFecha()");
        System.out.println(fecha_salida);
        System.out.println(factura.getFecha_ingreso());
    }
    
    public void onRowEdit(SigExistenciasBodega obj) {
        RequestContext.getCurrentInstance().update("formBode");
    }
    
    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Edicion Cancelada!", ((SigSalidaBodega) event.getObject()).getFk_codigo());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    public void onCellEdit(CellEditEvent event) {
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();

        if (newValue != null && !newValue.equals(oldValue)) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Registro Cambiado", "Anterior: " + oldValue + ", Nuevo:" + newValue);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }
    public void guardarLista() {
        if (salidaBO.guardarSalida(this,sessionbean)) {
            System.out.println("Se realizo la salida!");
        } else {
            FacesMessage msg = new FacesMessage("No se ha podido guardar los registros!");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            RequestContext.getCurrentInstance().update("formBode");
            Ajax.update("formBode");
        }
    }
    public void refreshPage() {
        try {
            FacesContext contex = FacesContext.getCurrentInstance();
            String path=FacesContext.getCurrentInstance().getExternalContext().getContextName();
            contex.getExternalContext().redirect(path+"?faces-redirect=true");
        } catch (Exception e) {
                System.out.println("No se realizo el refresh");
        }

    }
    public void confirm(){
        guardarLista(); 
        RequestContext.getCurrentInstance().execute("PF('successSal').show();");
    }
}
