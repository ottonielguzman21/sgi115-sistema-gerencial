/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.bodega.bean;

import sig.login.bean.SessionBean;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import lombok.Getter;
import lombok.Setter;
import sig.login.clase.Mensajes;
import sig.login.entity.SigCatalogoMarca;
import sig.bodega.bo.ExistenciaBodegaBO;


/**
 *
 * @author Mario
 */
@ManagedBean(name="marcaBean")
@ViewScoped
@Getter
@Setter
public class MarcaBean extends Mensajes{
    
    @ManagedProperty(value = "#{existenciaBodegaBO}")
    private ExistenciaBodegaBO existenciaBO;
    
    @ManagedProperty(value = "#{sessionBean}")
    private SessionBean sessionbean;
    /*@ManagedProperty(value = "#{adminCentroBO}")
    private AdminCentroBO adminCentroBO;*/
    private SigCatalogoMarca marca;
    private List<SigCatalogoMarca> listMarca;
    
    
    public MarcaBean(){
        marca = new SigCatalogoMarca();
    }
    
    @PostConstruct
    public void main() {
        marca= new SigCatalogoMarca();
        listMarca = existenciaBO.listMarcaTodos();
        
    }
    
    public void insertMarca(){
        if(existenciaBO.insertMarca(this, sessionbean)){
            mensaje(1, "Producto agregado correctamente");
            marca = new SigCatalogoMarca();
            listMarca = existenciaBO.listMarcaTodos();
           
        }else{
            mensaje(2, "Error al ingresar producto a Bodega");
        }
    }
    public void updateEstadoMarca(SigCatalogoMarca mrc) {
        mrc.setEstado(!mrc.isEstado());
        if (existenciaBO.updateMarca(mrc, sessionbean)) {
            listMarca = existenciaBO.listMarca();
            System.out.println("** existenciaMarcaImplBO ** insert marca ** " + java.util.Calendar.getInstance().getTime());

            mensaje(1, "Estado de la marca modificado correctamente");
        } else {
            mensaje(2, "Error al modificar el estado");
        }
    }
    public void deleteMenu(SigCatalogoMarca mrc) {
        if (existenciaBO.deleteMarca(mrc)) {
            listMarca = existenciaBO.listMarca();
            mensaje(1, "Marca eliminada correctamente");
        } else {
            mensaje(2, "Error al eliminar la marca");
        }
    }
    
    public void onRowEdit(SigCatalogoMarca mcr) {
        if (existenciaBO.updateMarca(mcr, sessionbean)) {
            mensaje(1, "Marca modificada correctamente");
        } else {
            mensaje(2, "Error al modificar la Marca");
        }
    }

    public void onRowCancel() {
        mensaje(2, "Modificación cancelada");
    }
    
}