/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.bodega.bean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.event.FileUploadEvent;
import sig.login.clase.Mensajes;

/**
 *
 * @author Alexis Lopez
 */
@ViewScoped
@ManagedBean
@Getter
@Setter
public class PruebaBean extends Mensajes{

    private String campo1;
    public PruebaBean() {
        campo1 = "hola mundo";
        System.out.println("******* SE CREO EL BEAN");
    }
    
    @PostConstruct
    public void main(){
        System.out.println("******** ENTRO EN POSTCONSTRUC");
    }
    /*SUBIR ARCHIVO*/
    public void subirArchivoExcel(FileUploadEvent event) {
        try {
            System.out.println("*********** ENTRO");
            System.out.println("*********** "+event);
            //archivo = new SubirArchivoServer();
            //urlArchivo = archivo.SubirArchivo(event, "/SIG/Excel/ArchivosSubidos/", "Cargar_Datos"); //SUBIR EL ARCHIVO
            //cargarDatos();
        } catch (Exception e) {
            System.out.println("Error en subir el archivo ************ " + e);
        }
    }
}
