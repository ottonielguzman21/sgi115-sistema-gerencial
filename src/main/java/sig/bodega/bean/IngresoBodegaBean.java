/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.bodega.bean;

import sig.login.bean.SessionBean;
import sig.dto.IngresoBodegaDto;
import sig.bodega.entity.SigExistenciasBodega;
import sig.negocio.producto.entity.SigProducto;
import sig.negocio.producto.bo.AdminProductoBO;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import lombok.Getter;
import lombok.Setter;
import sig.login.clase.Mensajes;
import sig.login.entity.SigCatalogoMarca;
import sig.bodega.bo.ExistenciaBodegaBO;
import sig.bodega.entity.Factura;
import sig.bodega.entity.SigIngresoAuditoria;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.MoveEvent;
import org.primefaces.event.RowEditEvent;
import javax.faces.event.ActionEvent;
import org.omnifaces.util.Ajax;

/**
 *
 * @author otoniel21
 */
@ManagedBean(name = "ingresoBodegaBean")
@ViewScoped
@Getter
@Setter
public class IngresoBodegaBean extends Mensajes {

    @ManagedProperty(value = "#{existenciaBodegaBO}")
    private ExistenciaBodegaBO ingresoBO;

    @ManagedProperty(value = "#{sessionBean}")
    private SessionBean sessionbean;

    @ManagedProperty(value = "#{adminProductoBO}")
    private AdminProductoBO adminProductoBO;
    //PARAMETROS INICIALES PARA ENTRADA DE PRODUCTO EXISTENTE
    private List<IngresoBodegaDto> listaDto;
    //END PARAMETROS INICIALES PARA ENTRADA DE PRODUCTO EXISTENTE
    private List<SigCatalogoMarca> listMrc;
    private List<SigProducto> listPro;
    private List<SigProducto> productos;
    public SigExistenciasBodega existenciaBodega;
    public Factura factura;
    private List<SigExistenciasBodega> listExistenciaBodega;//para el listener siguiente
    private List<SigExistenciasBodega> listaActualExistencias;
    private Date fecha_ingreso;
    private List<SigIngresoAuditoria> registrosIngreso;//registros de auditoria
    private List<SigIngresoAuditoria> regPersistir;//registros de auditoria a guardar
    private double total = 0.00;
    public Date currentDate = new Date();

    public IngresoBodegaBean() {
        existenciaBodega = new SigExistenciasBodega();

    }

    @PostConstruct
    public void main() {
        factura = new Factura();
        factura.setFecha_ingreso(new Date());
        factura.setFk_idCentro(sessionbean.getUsu().getSigCentro().getPkIdCentro());
        factura.setTipo_ingreso("");
        factura.setRealizado_por(sessionbean.getUsu().getNombre() + " " + sessionbean.getUsu().getApellido());
        listaDto = ingresoBO.listFiltradaCentro(factura.getFk_idCentro());
        listMrc = ingresoBO.listMarca();
        productos = adminProductoBO.listProducto();
        registrosIngreso = ingresoBO.registrosAudCentro(factura.getFk_idCentro());
        listExistenciaBodega = new ArrayList<>();
        regPersistir = new ArrayList<>();
        listaActualExistencias = ingresoBO.listaExistenciaBodegaPorCentro(sessionbean.getUsu().getSigCentro().getPkIdCentro());
    }

    public List<SigExistenciasBodega> ingresosBodegaUpdate() {
        if (factura.getNum_factura() >0 && factura.getNombre_distribuidor() != "" && factura.getFecha_ingreso().toString()!=null && listPro.isEmpty()!=true) {
            Ajax.update("formExis");
            RequestContext.getCurrentInstance().execute(" PF('ingresoProductos').hide(); PF('detalleIngPro').show();");
            listExistenciaBodega.clear();
            for (SigProducto item : listPro) {
                SigExistenciasBodega exisBodega = new SigExistenciasBodega();
                exisBodega.setFk_codigo(item);
                exisBodega.setFecha_ingreso(factura.getFecha_ingreso());//ok
                exisBodega.setFk_idCentro(factura.getFk_idCentro());//ok
                exisBodega.setNombre_distribuidor(factura.getNombre_distribuidor());//ok
                exisBodega.setNum_factura(factura.getNum_factura());//ok
                exisBodega.setTipo_ingreso(factura.getTipo_ingreso());//okok
                exisBodega.setPrecio_unit(0.00);//ok
                exisBodega.setCantidad(1);//ok
                exisBodega.setFecha_caducidad(factura.getFecha_ingreso());//llevaa la misma de ingreso, el usuario debe cambiarla
                exisBodega.setFecha_auditoria("asdfasd");//
                SigCatalogoMarca cMarca = new SigCatalogoMarca(1);
                exisBodega.setFk_marca(cMarca);
                exisBodega.setRealizado_por(factura.getRealizado_por());//ok
                exisBodega.setDescripcion("SIN DETALLES");
                total += exisBodega.getSubTotal();
                exisBodega.setSubTotal(exisBodega.getPrecio_unit() * exisBodega.getCantidad());
                SigIngresoAuditoria ingAud = new SigIngresoAuditoria();
                ingAud.setCantidad_aud(exisBodega.getCantidad());
                ingAud.setDescripcion_aud(exisBodega.getDescripcion());
                ingAud.setFecha_caducidad_aud(exisBodega.getFecha_caducidad());
                ingAud.setFecha_ingreso_aud(exisBodega.getFecha_ingreso());
                ingAud.setFecha_registro_aud(exisBodega.getFecha_ingreso());
                ingAud.setFk_codigo(exisBodega.getFk_codigo());
                ingAud.setFk_idCentro(exisBodega.getFk_idCentro());
                ingAud.setFk_marca(exisBodega.getFk_marca());
                ingAud.setNombre_distribuidor_aud(exisBodega.getNombre_distribuidor());
                ingAud.setNum_factura_aud(exisBodega.getNum_factura());
                ingAud.setPrecio_unit_aud(exisBodega.getPrecio_unit());
                ingAud.setRealizado_por_aud(exisBodega.getRealizado_por());
                ingAud.setTipo_ingreso_aud(exisBodega.getTipo_ingreso());

                listExistenciaBodega.add(exisBodega);
                regPersistir.add(ingAud);
            }
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERROR!, SELECCIONE AL MENOS UN PRODUCTO DEL CATALOGO...","sss"));
        }
        
        return listExistenciaBodega;
    }

    public void asignarFecha() {
        factura.setFecha_ingreso(fecha_ingreso);
        System.out.println("en asignarFecha()");
        System.out.println(fecha_ingreso);
        System.out.println(factura.getFecha_ingreso());
    }

    public void editarExistenciaSeleccionada(int index, SigExistenciasBodega exisBode) {
        listExistenciaBodega.set(index, exisBode);
    }
//    public void onRowEdit(RowEditEvent event) {

    public void onRowEdit(SigExistenciasBodega obj) {
        obj.setSubTotal(obj.getCantidad() * obj.getPrecio_unit());
        System.out.println("subtotal: " + obj.getSubTotal());
        RequestContext.getCurrentInstance().update("formBode");
    }

    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Edicion Cancelada!", ((SigExistenciasBodega) event.getObject()).getFk_codigo().getCodigo());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onCellEdit(CellEditEvent event) {
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();
        if (newValue != null && !newValue.equals(oldValue)) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Registro Cambiado", "Anterior: " + oldValue + ", Nuevo:" + newValue);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }
//    validaciones funcionales

    public void validar(FacesContext context, UIComponent toValide, Object value) {
        context = FacesContext.getCurrentInstance();
        String texto = (String) value;
        if (!texto.equalsIgnoreCase("F") && !texto.equalsIgnoreCase("M")) {
            ((UIInput) toValide).setValid(false);
            context.addMessage(toValide.getClientId(context), new FacesMessage("sexo no validado"));

        }
    }

    public void guardarLista() {
        if (ingresoBO.guardarListaIngresos(this, sessionbean)) {
            System.out.println("Ingresos exitosos");
        } else {
            RequestContext.getCurrentInstance().update("formBode");
        }
    }

    public void cancelar() throws IOException {
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
    }

    //confirm dialog
    public void handleClose(CloseEvent event) { 
        addMessage(event.getComponent().getId() + " Cerrado", "So you don't like nature?");
    }

    public void handleMove(MoveEvent event) {
        addMessage(event.getComponent().getId() + " Movido", "Left: " + event.getLeft() + ", Top: " + event.getTop());
    }

    public void confirmar() throws InterruptedException {
        guardarLista();
        regPersistir.clear();
        for (SigExistenciasBodega exisBodega : listExistenciaBodega) {
            SigIngresoAuditoria ingAud = new SigIngresoAuditoria();
            ingAud.setCantidad_aud(exisBodega.getCantidad());
            ingAud.setDescripcion_aud(exisBodega.getDescripcion());
            ingAud.setFecha_caducidad_aud(exisBodega.getFecha_caducidad());
            ingAud.setFecha_ingreso_aud(exisBodega.getFecha_ingreso());
            ingAud.setFecha_registro_aud(exisBodega.getFecha_ingreso());
            ingAud.setFk_codigo(exisBodega.getFk_codigo());
            ingAud.setFk_idCentro(exisBodega.getFk_idCentro());
            ingAud.setFk_marca(exisBodega.getFk_marca());
            ingAud.setNombre_distribuidor_aud(exisBodega.getNombre_distribuidor());
            ingAud.setNum_factura_aud(exisBodega.getNum_factura());
            ingAud.setPrecio_unit_aud(exisBodega.getPrecio_unit());
            ingAud.setRealizado_por_aud(exisBodega.getRealizado_por());
            ingAud.setTipo_ingreso_aud(exisBodega.getTipo_ingreso());
            regPersistir.add(ingAud);

        }
        ingresoBO.guardarIngAud(regPersistir);
    }
    public void actTableRegistros(){
        registrosIngreso = ingresoBO.registrosAudCentro(factura.getFk_idCentro());
    }
    
    public void addMessage(String summary, String detail) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public void refreshPage() {
        try {
            FacesContext contex = FacesContext.getCurrentInstance();
            String path = FacesContext.getCurrentInstance().getExternalContext().getContextName();
            contex.getExternalContext().redirect(path + "?faces-redirect=true");
        } catch (Exception e) {
            System.out.println("No se pudo realizar el ingreso!");
        }

    }

    public void calcularTotal() {
        total = 0.00;
        for (Iterator<SigExistenciasBodega> it = listExistenciaBodega.iterator(); it.hasNext();) {
            SigExistenciasBodega exis = it.next();
            total += (exis.getCantidad() * exis.getPrecio_unit());
        }
        RequestContext.getCurrentInstance().update("formExis:totalForm");
    }
}
