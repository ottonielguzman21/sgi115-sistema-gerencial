package sig.bodega.bean;

import sig.bodega.bo.DescargoBodegaBO;
import sig.login.bean.SessionBean;
import sig.login.entity.SigCentro;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import lombok.Getter;
import lombok.Setter;
import sig.login.clase.Mensajes;
import sig.login.entity.SigCatalogoMarca;
import sig.bodega.entity.SigDescargoBodega;
import sig.bodega.entity.SigExistenciasBodega;
import java.text.DateFormat;
import java.util.Date;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Otoniel
 */
@ManagedBean(name = "descargoBodegaBean")
@ViewScoped
@Getter
@Setter
public class DescargoBodegaBean extends Mensajes {

    @ManagedProperty(value = "#{descargoBodegaBO}")
    private DescargoBodegaBO descargoBO;
    @ManagedProperty(value = "#{ingresoBodegaBean}")
    private IngresoBodegaBean ingresoBO;

    @ManagedProperty(value = "#{sessionBean}")
    private SessionBean sessionbean;

    //public String ent_a;
    public Date fechaDescargo;
    public String nomCompl;
    private int cod_ingreso;
    private SigExistenciasBodega existencia;
    private List<SigCatalogoMarca> listMrc;
    private List<SigCentro> listCentro;
    private List<SigDescargoBodega> listDescargoBodega;
    private SigDescargoBodega descargoBodega;//usado
    private List<SigExistenciasBodega> listExist;
    private SigExistenciasBodega listExistSelec;
    private SigExistenciasBodega exisBodega;//usado
    private int cantDescargar = 0;
    private int numAcuerdo = 0;
    private String Justificacion = null;
    private String realizadoPor = null;
    private List<SigDescargoBodega> listaHistorialDescargos;

    public DescargoBodegaBean() {
        descargoBodega = new SigDescargoBodega();
    }

    @PostConstruct
    public void main() {
        int idCentro = sessionbean.getUsu().getSigCentro().getPkIdCentro();
        listaHistorialDescargos = descargoBO.listaHistorialDescargos(idCentro);
        descargoBodega = new SigDescargoBodega();
        listExist = descargoBO.listDetalleIngreso(idCentro);
        exisBodega = new SigExistenciasBodega();//oto
        fechaDescargo = new Date();
        realizadoPor = sessionbean.getUsu().getNombre() + " " + sessionbean.getUsu().getApellido();
    }

    public SigExistenciasBodega descargarBodega(SigExistenciasBodega e) {
        exisBodega = e;
        System.out.println(exisBodega.toString());
        return exisBodega;
    }

    public void realizarDescargo() {
        descargoBodega.setCantidad(cantDescargar);
        String jUpper = Justificacion.toUpperCase();
        descargoBodega.setJustificacion(jUpper);
        descargoBodega.setFecha_auditoria(DateFormat.getDateInstance().format(fechaDescargo));
        descargoBodega.setFecha_descargo(fechaDescargo);
        descargoBodega.setFk_ExistenciaBodega(exisBodega);
        descargoBodega.setNumAcuerdo(numAcuerdo);
        descargoBodega.setFk_centro(sessionbean.getUsu().getSigCentro().getPkIdCentro());
        descargoBodega.setFk_producto(exisBodega.getFk_codigo().getCodigo());
        descargoBodega.setRealizado_por(realizadoPor);
        
        if (descargoBodega.getCantidad() != 0 && descargoBodega.getCantidad()<=exisBodega.getCantidad()) {
            descargoBO.guardarDescargo(descargoBodega, sessionbean);
            //refreshPage();
            mensaje(1,"DESCARGO REALIZADO CON EXITO!, POR FAVOR VERIFIQUE EL HISTORIAL DE DESCARGOS REALIZADOS...");
            RequestContext context = RequestContext.getCurrentInstance();
            context.update("formConfirmarRegistro:growl2");
            actualizarTabla();
            context.update("tblDescargos");
//            context.execute("PF('descargosH').show();"); quise que lo mostrara pero no actualizaba la  tabla.
            context.execute("PF('successDesc').show();");
        }else{
            mensaje(2, "POR FAVOR INGRESE UNA CANTIDAD SUPERIOR A CERO Y MENOR O IGUAL QUE SU MAXIMO");
            RequestContext.getCurrentInstance().update("formConfirmarRegistro:growl2");
        }
    }

    public void refreshPage() {
        try {
            FacesContext contex = FacesContext.getCurrentInstance();
            String path = FacesContext.getCurrentInstance().getExternalContext().getContextName();
            contex.getExternalContext().redirect(path + "?faces-redirect=true");
        } catch (Exception e) {
            System.out.println("Me voy al carajo no funciono!");
        }
    }
    public void actualizarTabla(){
        listExist = descargoBO.listDetalleIngreso(sessionbean.getUsu().getSigCentro().getPkIdCentro());
    }
}