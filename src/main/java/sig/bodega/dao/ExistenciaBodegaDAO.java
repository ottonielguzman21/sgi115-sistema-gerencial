/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.bodega.dao;

import sig.dto.IngresoBodegaDto;
import sig.login.entity.SigCatalogoMarca;
import sig.bodega.entity.SigExistenciasBodega;
import sig.bodega.entity.SigIngresoAuditoria;
import java.util.List;

/**
 *
 * @author Mario
 */
public interface ExistenciaBodegaDAO {
    public void guardarIngreso(SigExistenciasBodega existencia);
    
    //LISTADO BODEGA
    List<SigExistenciasBodega> listBodega();
    public List<IngresoBodegaDto> listFiltradaCentro(int idCentro);
    
    //LISTADO MARCA
    public List<SigCatalogoMarca> listMarca();
    public List<SigCatalogoMarca> listMarcaProBodega(String pro);
    public List<SigCatalogoMarca> listMarcaAll();
    //OBTENGO LA LISTA POR CENTRO
    public List<SigExistenciasBodega> listaExistenciaBodegaPorCentro(int pkIdCentro);

    public List<SigIngresoAuditoria> registrosAudCentro(int fk_idCentro);
    
}
