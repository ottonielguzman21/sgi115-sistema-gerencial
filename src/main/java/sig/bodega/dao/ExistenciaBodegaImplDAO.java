/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.bodega.dao;

import sig.dto.IngresoBodegaDto;
import sig.login.entity.SigCatalogoMarca;
import sig.bodega.entity.SigExistenciasBodega;
import sig.bodega.entity.SigIngresoAuditoria;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Usuario
 */
@Transactional(value = "TMA")
@Service(value = "existenciaDAO")
public class ExistenciaBodegaImplDAO extends HibernateDaoSupport implements ExistenciaBodegaDAO {

    @Autowired
    public ExistenciaBodegaImplDAO(@Qualifier(value = "SFA") SessionFactory sf) {
        super.setSessionFactory(sf);
    }

    @Override
    public void guardarIngreso(SigExistenciasBodega existencia) {
        currentSession().persist(existencia);
    }

    @Override
    public List<SigExistenciasBodega> listBodega() {
        return currentSession().createCriteria(SigExistenciasBodega.class)
                .addOrder(Order.asc("pk_ExistenciaBodega"))
                .list();
    }

    @Override
    public List<IngresoBodegaDto> listFiltradaCentro(int idCentro) {
        return currentSession().createQuery("select new sig.dto.IngresoBodegaDto(i.pk_ExistenciaBodega, p.codigo, p.nombre, p.descrip, sum(i.cantidad), p.presentacion, u.nombre) "
                + "from SigExistenciasBodega i inner join i.fk_codigo p "
                + "inner join p.sigUnidadmedida u "
                + "where i.fk_idCentro=:id and i.cantidad > 0 "
                + "group by i.fk_codigo").setParameter("id", idCentro).list();
    }

    @Override
    public List<SigIngresoAuditoria> registrosAudCentro(int fk_idCentro) {
        return currentSession().createCriteria(SigIngresoAuditoria.class)
                .addOrder(Order.desc("pk_IngresoBodega_aud"))
                .add(Restrictions.eq("fk_idCentro", fk_idCentro))
                .list();
    }

    @Override
    public List<SigExistenciasBodega> listaExistenciaBodegaPorCentro(int pkIdCentro) {
        return currentSession().createCriteria(SigExistenciasBodega.class)
                .addOrder(Order.desc("pk_ExistenciaBodega"))
                .add(Restrictions.eq("fk_idCentro", pkIdCentro))
                .add(Restrictions.sqlRestriction("cantidad > 0"))
                .list();
    }

    @Override
    public List<SigCatalogoMarca> listMarca() {
        return currentSession().createCriteria(SigCatalogoMarca.class)
                .addOrder(Order.asc("nombre"))
                .add(Restrictions.sqlRestriction("estado=1 and pk_marca>0"))
                .list();
    }

    @Override
    public List<SigCatalogoMarca> listMarcaProBodega(String pro) {
        return currentSession().createQuery("SELECT DISTINCT new SigCatalogoMarca(mar.codigoMarca, mar.nombre, mar.descripcion, mar.estado) FROM "
                + "SigCatalogoMarca mar,SigExistenciasBodega exis,SigProducto pro WHERE mar.codigoMarca = exis.fk_marca AND pro.codigo = exis.fk_codigo AND pro.codigo=:id order by mar.nombre asc")
                .setParameter("id", pro)
                .list();

    }
    
    
    @Override
    public List<SigCatalogoMarca> listMarcaAll() {
        return currentSession().createCriteria(SigCatalogoMarca.class)
                .addOrder(Order.asc("nombre"))
                .add(Restrictions.sqlRestriction("pk_cod_marca<>1"))
                .list();
    }

}
