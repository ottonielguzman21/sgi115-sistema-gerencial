/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.bodega.dao;

//import sig.login.entity.SigCatalogoMarca;

import sig.dto.IngresoBodegaDto;
import sig.dto.SalidaBodegaDto;
import sig.bodega.entity.SigExistenciasBodega;
import sig.bodega.entity.SigSalidaBodega;
import java.util.List;

/**
 *
 * @author Usuario
 */
public interface SalidaBodegaDAO {
    public void guardarSalida(SigSalidaBodega salida);
    
    //LISTADO SALIDA BODEGA
    List<SigSalidaBodega> listSalidaBodega(int pkIdCentro);
    List<SigExistenciasBodega> listExistBodega();
    
    //LISTADO DETALLE DEL PRODUCTO (NO SE OCUPA)
    public List<SalidaBodegaDto> listProductoCentro(int idCentro);
    public List<SigExistenciasBodega> listDetalleProducto(int idCentro, String fk_codigo);
    
    //OBTIENE EL LISTADO DEL PRIMER PRODUCTO INGRESADO
    public List<SigExistenciasBodega> connsultaPEPS(int idCentro);

     
}
