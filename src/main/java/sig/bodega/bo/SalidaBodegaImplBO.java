/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.bodega.bo;

import sig.dto.IngresoBodegaDto;
import sig.dto.SalidaBodegaDto;
import sig.bodega.bean.SalidaBodegaBean;
import sig.login.dao.CrudDAO;
import java.util.List;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sig.bodega.dao.ExistenciaBodegaDAO;
import sig.bodega.dao.SalidaBodegaDAO;
import sig.bodega.entity.SigExistenciasBodega;
import sig.bodega.entity.SigSalidaBodega;
import sig.login.bean.SessionBean;

/**
 *
 * @author Gonzalez
 */
@Getter
@Service(value = "salidaBodegaBO")
public class SalidaBodegaImplBO implements SalidaBodegaBO {
    @Autowired
    private SalidaBodegaDAO salidaDao;
    @Autowired
    private ExistenciaBodegaDAO ingresoDao;
    @Autowired
    private CrudDAO crudDAO;
    
    @Override
    public List<SigSalidaBodega> listSalidaBodega(int pkIdCentro) {
        return salidaDao.listSalidaBodega(pkIdCentro);
    }

    @Override
    public List<SalidaBodegaDto> listProductoCentro(int idCentro) {
        return salidaDao.listProductoCentro(idCentro);
    }

    @Override
    public List<SigExistenciasBodega> listDetalleProducto(int idCentro, String fk_codigo) {
    return salidaDao.listDetalleProducto(idCentro, fk_codigo);
            }

    @Override
    public List<SigExistenciasBodega> connsultaPEPS(int idCentro) {
        return salidaDao.connsultaPEPS(idCentro);
    }

    @Override
    public List<SigExistenciasBodega> listExistBodega() {
        return salidaDao.listExistBodega();
    }

    @Override
    public boolean guardarSalida(SalidaBodegaBean salida, SessionBean usu) {
        try{
            List<SigSalidaBodega> lista=salida.getListSalidaBodega();
            for (SigSalidaBodega existencias : lista) {
                crudDAO.insertObj(existencias);

                crudDAO.scriptSQL("insert into sig_bitacora (fecha, fk_usuario, fk_rol, fk_centro, tabla, accion, informacion) Values(NOW(),'"+usu.getUsu().getCodigo()+"', '"+usu.getUsu().getSigRol().getPkRol()+"', '"+usu.getUsu().getSigCentro().getPkIdCentro()+"','sig_salida_bodega','SALIDA', '"+existencias.toString()+"')");
                int entrada = Integer.valueOf(crudDAO.scriptSQLKardex("SELECT ifnull(SUM(entrada),0) FROM sig_kardex_bodega WHERE fk_producto="+existencias.getFk_ExistenciaBodega().getFk_codigo().getCodigo()+""));
                int salida2 = Integer.valueOf(crudDAO.scriptSQLKardex("SELECT ifnull(SUM(salida),0) FROM sig_kardex_bodega WHERE fk_producto="+existencias.getFk_ExistenciaBodega().getFk_codigo().getCodigo()+""));
                int saldo = entrada-salida2-existencias.getCantidad();
                crudDAO.scriptSQL("insert into sig_kardex_bodega (fk_producto, fk_marca, fecha, precio_uni, salida, fk_centro, saldo) Values('"+existencias.getFk_ExistenciaBodega().getFk_codigo().getCodigo()+"',"+existencias.getFk_ExistenciaBodega().getFk_marca().getCodigoMarca()+", '"+new java.sql.Date(existencias.getFecha_salida().getTime())+"', "+existencias.getFk_ExistenciaBodega().getPrecio_unit()+","+existencias.getCantidad()+", "+existencias.getFk_idCentro()+","+saldo+")");
            }
            return true;
        }catch(Exception e){
            System.out.println("Ha ocurrido un problema al guardar la lista de Existencias de bodega!");
            return false;
        }
    }

    
}
