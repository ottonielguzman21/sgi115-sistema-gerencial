package sig.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Gonzalez
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SalidaBodegaDto {
    
    private int pk_Existencia_Bodega;
    private String fk_producto;
    private String nombre;
    private String descripcion;
    private long cantidad;
    private String presentacion;   
    private String unidadMedida;
    
}
