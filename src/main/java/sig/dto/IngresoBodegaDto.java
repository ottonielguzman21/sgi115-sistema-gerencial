package sig.dto;

//import sig.login.entity.sigCatalogoMarca;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author otoniel21
 */

public class IngresoBodegaDto {
    
    private int pk_Existencia_Bodega;
    private String fk_producto;
    private String nombre;
    private String descripcion;
    private long cantidad;
    private String presentacion;   
    private String unidadMedida;

    public IngresoBodegaDto(int pk_ExistenciaBodega, String fk_producto, String nombre, String descripcion, long cantidad, String presentacion, String unidadMedida) {
        this.pk_Existencia_Bodega = pk_ExistenciaBodega;
        this.fk_producto = fk_producto;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.cantidad = cantidad;
        this.presentacion = presentacion;
        this.unidadMedida = unidadMedida;
    }

    public IngresoBodegaDto() {
    }

    public IngresoBodegaDto(int pk_ExistenciaBodega, String fk_codigo, String nombre, String descripcion, long cantidad, String presentacion) {
        this.pk_Existencia_Bodega = pk_ExistenciaBodega;
        this.fk_producto = fk_codigo;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.cantidad = cantidad;
        this.presentacion = presentacion;
    }
    
    public IngresoBodegaDto(int pk_ExistenciaBodega, String nombre, String descripcion, long cantidad, String presentacion, String unidadMedida) {
        this.pk_Existencia_Bodega = pk_ExistenciaBodega;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.cantidad = cantidad;
        this.presentacion = presentacion;
        this.unidadMedida = unidadMedida;
    }

    public int getPk_ExistenciaBodega() {
        return pk_Existencia_Bodega;
    }

    public void setPk_ExistenciaBodega(int pk_ExistenciaBodega) {
        this.pk_Existencia_Bodega = pk_ExistenciaBodega;
    }

    public String getFk_codigo() {
        return fk_producto;
    }

    public void setFk_codigo(String fk_codigo) {
        this.fk_producto = fk_codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public long getCantidad() {
        return cantidad;
    }

    public void setCantidad(long cantidad) {
        this.cantidad = cantidad;
    }

    public String getPresentacion() {
        return presentacion;
    }

    public void setPresentacion(String presentacion) {
        this.presentacion = presentacion;
    }

    public String getUnidadMedida() {
        return unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

        
    
}
