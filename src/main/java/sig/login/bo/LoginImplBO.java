/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.login.bo;

import sig.login.bean.LoginBean;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import lombok.Getter;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sig.login.dao.LoginDAO;
import sig.login.entity.SigMenu;
import sig.login.entity.SigUsuario;

/**
 *
 * @author Administrador
 */
@Service(value = "loginBO")
@Getter
public class LoginImplBO implements LoginBO {

    @Autowired
    private LoginDAO loginDAO;

    private DefaultMenuItem item;

    @Override
    public boolean validarUsuario(LoginBean bean) {
        try {
            SigUsuario usu = loginDAO.consultarUsuario(bean.getUsuario(), bean.getClave());
            if (usu != null) {
                bean.getSessionBean().setUsu(usu);
                /*MENU*/
                bean.getSessionBean().setModel(new DefaultMenuModel());
                DefaultSubMenu firstSubmenu = new DefaultSubMenu();
                List<SigMenu> lm = loginDAO.listMenu(usu.getSigRol().getPkRol(), -1);
                if (lm.size() > 0) {
                    for (SigMenu m : lm) {
                        if (m.getRutaRaiz() != null) {
                            item = new DefaultMenuItem(m.getNomVista());
                            if (((ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext())
                                    .getRealPath(m.getRutaRaiz() + m.getNomArchivo() + ".xhtml") == null) {
                                item.setOutcome("/secure/Error/mantenimiento.sig");
                            } else {
                                item.setOutcome(m.getRutaRaiz() + m.getNomArchivo() + ".sig");
                            }
                            if (m.getSigIcono() != null) {
                                item.setIcon("fa " + m.getSigIcono().getNombre());
                            }
//                            firstSubmenu.addElement(item);
                            bean.getSessionBean().getModel().addElement(item);

                        } else {
                            firstSubmenu = new DefaultSubMenu();
                            firstSubmenu.setLabel(m.getNomVista());
                            consultarSubMenu(usu.getSigRol().getPkRol(), firstSubmenu, m.getCodigo());
                            bean.getSessionBean().getModel().addElement(firstSubmenu);
                        }

                        
                    }
                    return true;
                } else {
                    if (((ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext())
                            .getRealPath("/secure/Error/mantenimiento.sig".replace(".sig", ".xhtml")) != null) {
                        item = new DefaultMenuItem("No hay paginas");
                        item.setOutcome("/secure/Error/mantenimiento.sig");
                        item.setIcon(null);
                        firstSubmenu.addElement(item);
                        bean.getSessionBean().getModel().addElement(firstSubmenu);
                        return true;
                    }
                }
            }
            return false;
        } catch (Exception e) {
            System.out.println("******* " + e);
            return false;
        }
    }

    private void consultarSubMenu(int rol, DefaultSubMenu firstSubmenu, int codPadre) {
        for (SigMenu m : loginDAO.listMenu(rol, codPadre)) {
            if (m.getRutaRaiz() != null) {
                item = new DefaultMenuItem(m.getNomVista());
                if (((ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext())
                        .getRealPath(m.getRutaRaiz() + m.getNomArchivo() + ".xhtml") == null) {
                    item.setOutcome("/secure/Error/mantenimiento.sig");
                } else {
                    item.setOutcome(m.getRutaRaiz() + m.getNomArchivo() + ".sig");
                }
                if (m.getSigIcono() != null) {
                    item.setIcon("fa " + m.getSigIcono().getNombre());
                }
                firstSubmenu.addElement(item);

            } else {
//                firstSubmenu = new DefaultSubMenu();
                DefaultSubMenu sm = new DefaultSubMenu(m.getNomVista());
                firstSubmenu.addElement(sm);
                consultarSubMenu(rol, sm, m.getCodigo());
            }
        }
    }
    
}
