/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.login.bo;

import java.util.List;
import lombok.Getter;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sig.login.dao.BuscadorDAO;
import sig.login.dao.CrudDAO;
import sig.login.entity.SigIcono;

/**
 *
 * @author User
 */
@Getter
@Service(value = "buscadorBO")
public class BuscadorImplBO implements BuscadorBO{
    @Autowired
    private BuscadorDAO buscadorDAO;
    @Autowired
    private CrudDAO crudDAO;

  

    @Override
    public List<SigIcono> listIcono() {
        return buscadorDAO.listIcono();
    }

    @Override
    public boolean insertIcono(SigIcono ico) {
        try {
            crudDAO.insertObj(ico);
            return true;
        } catch (Exception e) {
            System.out.println("*** ERROR EN BUSCADOR IMPL BO(INSERT ICONO) **** "+e);
            return false;
        }
    }

    @Override
    public boolean updateIcono(SigIcono ico) {
        try {
            crudDAO.updateObj(ico);
            return true;
        } catch (Exception e) {
            System.out.println("**** ERROR EN UPDATE ICONO IMPL:BO *** "+e);
            return false;
        }
    }

    @Override
    public String consultarParametro(int cod) {
        return crudDAO.consultarParametro(cod);
    }
}
