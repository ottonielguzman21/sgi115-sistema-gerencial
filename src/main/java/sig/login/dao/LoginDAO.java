/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.login.dao;

import java.util.List;
import sig.login.entity.SigMenu;
import sig.login.entity.SigUsuario;

/**
 *
 * @author Administrador
 */
public interface LoginDAO {
    SigUsuario consultarUsuario(String usu,String cla);
    List<SigMenu> listMenu(int fkRol,int cod);
    
    //consultar usuario considerando su centro
    SigUsuario consultarUsuario(String usu,String cla, int centro);
    
}
