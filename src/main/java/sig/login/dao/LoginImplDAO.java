/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.login.dao;

import java.util.List;
import org.apache.commons.codec.digest.DigestUtils;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sig.login.entity.SigMenu;
import sig.login.entity.SigUsuario;

/**
 *
 * @author Administrador
 */
@Transactional(value = "TMA")
@Service(value = "loginDAO")
public class LoginImplDAO extends HibernateDaoSupport implements LoginDAO {

    private List l;

    @Autowired
    public LoginImplDAO(@Qualifier("SFA") SessionFactory sf) {
        super.setSessionFactory(sf);
    }

    @Override
    public SigUsuario consultarUsuario(String cod, String clav) {
        System.out.println("********** "+cod);
        System.out.println("********** "+DigestUtils.md5Hex(clav));
        l = currentSession().createCriteria(SigUsuario.class)
                .add(Restrictions.eq("codigo", cod.toUpperCase()))
                .add(Restrictions.eq("clave", DigestUtils.md5Hex(clav)))
                .add(Restrictions.eq("estado", true))
                .list();
        if (l.size() > 0) {
            return (SigUsuario) l.get(0);
        }
        return null;
    }

    @Override
    public List<SigMenu> listMenu(int fkRol,int cod) {
        return currentSession().createCriteria(SigMenu.class,"menu")
                .createAlias("menu.sigPermisos", "per")
                .add(Restrictions.eq("per.id.fkRol", fkRol))
                .add(Restrictions.sqlRestriction(cod == -1?" this_.fk_menu is null ":(" this_.fk_menu = '"+cod+"' ")))
                .addOrder(Order.asc("per.orden"))
                .list();
    }

    @Override
    public SigUsuario consultarUsuario(String cod, String cla, int centro) {
        System.out.println("********** "+cod);
        System.out.println("********** "+DigestUtils.md5Hex(cla));
        l = currentSession().createCriteria(SigUsuario.class)
                .add(Restrictions.eq("codigo", cod.toUpperCase()))
                .add(Restrictions.eq("fk_centro", centro))
                .add(Restrictions.eq("clave", DigestUtils.md5Hex(cla)))
                .add(Restrictions.eq("estado", true))
                .list();
        if (l.size() > 0) {
            return (SigUsuario) l.get(0);
        }
        return null;    }

}
