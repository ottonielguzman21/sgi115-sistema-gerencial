/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.login.dao;

import sig.login.entity.SigCentro;
import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sig.login.entity.SigIcono;

/**
 *
 * @author User
 */
@Service(value = "buscadorDAO")
@Transactional(value = "TMA")
public class BuscadorImplDAO extends HibernateDaoSupport implements BuscadorDAO{
    @Autowired
    public BuscadorImplDAO(@Qualifier(value = "SFA")SessionFactory sf) {
        super.setSessionFactory(sf);
    }

    @Override
    public List<SigIcono> listIcono() {
        return currentSession().createCriteria(SigIcono.class)
                .addOrder(Order.asc("nombre"))
                .list();
    } 

    @Override
    public List<SigCentro> listCentro() {
        return currentSession().createCriteria(SigCentro.class)
                .addOrder(Order.asc("nomCentro"))
                .list();
    }
    
}
