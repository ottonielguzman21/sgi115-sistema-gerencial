/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.login.dao;

import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sig.login.entity.SigParametro;

/**
 *
 * @author Administrador
 */
@Transactional(value = "TMA")
@Service(value = "crudDAO")
public class CrudImplDAO extends HibernateDaoSupport implements CrudDAO{
    @Autowired
    public CrudImplDAO(@Qualifier(value = "SFA")SessionFactory sf) {
        super.setSessionFactory(sf);
    }

    @Override
    public void insertObj(Object obj) {
        currentSession().save(obj);
    }

    @Override
    public void updateObj(Object obj) {
        currentSession().update(obj);
    }

    @Override
    public void deleteObj(Object obj) {
        currentSession().delete(obj);
    }

    @Override
    public String maxTblSql(String sql) {
        return String.valueOf(currentSession().createSQLQuery(sql).list().get(0));
    }

    @Override
    public String maxRubro(String sql) {
        return String.valueOf(currentSession().createSQLQuery(sql).list().get(0));
    }
    
    @Override
    public String idMenu(String sql) {
        return String.valueOf(currentSession().createSQLQuery(sql).list().get(0));
    }
    
    @Override
    public String consultarParametro(int cod) {
        List l = currentSession().createCriteria(SigParametro.class)
                .add(Restrictions.eq("codigo", cod))
                .list();
        if(l.size()>0){
            return ((SigParametro)l.get(0)).getValor();
        }
        return null;
    }

    @Override
    public void scriptSQL(String sql) {
        currentSession().createSQLQuery(sql).executeUpdate();
    }
    
    @Override
    public String scriptSQLKardex(String sql) {
        return String.valueOf(currentSession().createSQLQuery(sql).list().get(0));
    }

    @Override
    public void insertUpdateObj(Object obj) {
        currentSession().saveOrUpdate(obj);
    }
    
}
