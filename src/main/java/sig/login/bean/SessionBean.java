/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.login.bean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.MenuModel;
import sig.login.clase.Mensajes;
import sig.login.entity.SigUsuario;

/**
 *
 * @author Administrador
 */
@ManagedBean
@SessionScoped
@Getter
@Setter
public class SessionBean extends Mensajes {

    private SigUsuario usu;
    private MenuModel model;
    

    public SessionBean() {
    }

    public String cerrarSession() {
        HttpSession httpSession = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        httpSession.invalidate();
        usu = new SigUsuario();
        model = new DefaultMenuModel();
        return "/login.sig" + "?faces-redirect=true";
    }
    
}
