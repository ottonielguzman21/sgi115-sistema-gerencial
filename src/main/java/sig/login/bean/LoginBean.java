/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.login.bean;

import sig.login.clase.Mensajes;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import lombok.Getter;
import lombok.Setter;
import sig.login.bo.LoginBO;

/**
 *
 * @author Administrador
 */
@ManagedBean
//@RequestScoped
@ViewScoped
@Getter
@Setter
    public class LoginBean extends Mensajes{

    @ManagedProperty(value = "#{loginBO}")
    private LoginBO loginBO;

    @ManagedProperty(value = "#{sessionBean}")
    private SessionBean sessionBean;

    private String usuario;
    private String clave;
        
    public LoginBean() {
        
    }

    public String validaLogin() {
        if (loginBO.validarUsuario(this)) {
            HttpSession httpSession = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            httpSession.setAttribute("codigoLogeado", usuario.toUpperCase());
            return "/secure/default.ues"+"?faces-redirect=true";
        } else {
            usuario = null;
            clave = null;
              mensaje(2, "Usuario o contraseña incorrecta");
            return "login.xhtml";
        }
    }

}
