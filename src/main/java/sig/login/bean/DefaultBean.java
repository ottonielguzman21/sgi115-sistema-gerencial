/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.login.bean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.codec.digest.DigestUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultSubMenu;
import sig.login.bo.AdminUsuarioBO;
import sig.login.clase.Mensajes;

/**
 *
 * @author User
 */
@ManagedBean
@ViewScoped
@Getter
@Setter
public class DefaultBean extends Mensajes {

    @ManagedProperty(value = "#{sessionBean}")
    private SessionBean sessionBean;

    @ManagedProperty(value = "#{adminUsuarioBO}")
    private AdminUsuarioBO adminUsuarioBO;

    private String nuevaClave;
    private String nuevaClaveCon;

    public DefaultBean() {
    }

    public String cambioClave() {
        if (sessionBean.getUsu().getClave().equalsIgnoreCase("e26c062fedf6b32834e4de93f9c8b644")) {
            RequestContext.getCurrentInstance().update("formCCP");
            RequestContext.getCurrentInstance().execute("PF('updCCP').show();");
            return "";
        } else {
            if (sessionBean.getModel().getElements().get(0).getClass().equals(DefaultMenuItem.class)) {
                return ((DefaultMenuItem) sessionBean.getModel().getElements().get(0)).getOutcome() + "?faces-redirect=true";
            }else{
                return ((DefaultMenuItem) ((DefaultSubMenu) sessionBean.getModel().getElements().get(0)).getElements().get(0)).getOutcome() + "?faces-redirect=true";
            }
        }
    }

    public String modificarClave() {
        if (!nuevaClave.equalsIgnoreCase("nuevo")) {
            if (nuevaClave.equals(nuevaClaveCon)) {
                sessionBean.getUsu().setClave(DigestUtils.md5Hex(nuevaClave));
                if (adminUsuarioBO.updateUsuario(sessionBean.getUsu(), sessionBean)) {
                    mensaje(1, "Clave modificada correctamente");
                    RequestContext.getCurrentInstance().execute("PF('updCCP').hide();");
                    return ((DefaultMenuItem) ((DefaultSubMenu) sessionBean.getModel().getElements().get(0)).getElements().get(0)).getOutcome() + "?faces-redirect=true";
                } else {
                    mensaje(2, "Error al modificar su clave");
                }
            } else {
                mensaje(2, "Error las claves no considen");
            }
        } else {
            mensaje(2, "Error la clave no puede ser nuevo");
        }
        return "";
    }
}
