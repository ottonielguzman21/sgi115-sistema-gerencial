/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.login.bean;

import sig.login.bo.AdminUsuarioBO;
import sig.login.clase.Mensajes;
import sig.login.entity.SigCentro;
import sig.login.entity.SigUbicacion;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.context.RequestContext;

/**
 *
 * @author otoniel21
 */
@ManagedBean
@ViewScoped
@Getter
@Setter
public class CentroBean extends Mensajes {

    @ManagedProperty(value = "#{adminUsuarioBO}")
    private AdminUsuarioBO adminUsuarioBO;
    @ManagedProperty(value = "#{sessionBean}")
    private SessionBean sessionbean;
    private SigCentro sigCentro;
    private String deptoSelect;
    private String departamento;
    private String editDepto;
    private List<SigCentro> listCentro;
    private List<SigUbicacion> listaPorDepto;
    private List<SigUbicacion> listaPorDepto2;

    public CentroBean() {
        sigCentro = new SigCentro();
    }

    @PostConstruct
    public void main() {
        sigCentro = new SigCentro();
        sigCentro.setEstado(true);
        listCentro = adminUsuarioBO.listCentro();
        listaPorDepto2 = adminUsuarioBO.listPorDepto(departamento);
    }

    public void displayLocation() {
        FacesMessage msg;
        if (departamento != null && listaPorDepto != null) {
            msg = new FacesMessage("Se selecciono", departamento + " de ");
        } else {
            msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Invalid", "Municipio no Seleccionado!");
        }

        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void onDeptoCambie() {
        if (!departamento.equals("")) {
            listaPorDepto = adminUsuarioBO.listPorDepto(departamento);
        }
    }

    public void onDeptoEdit() {
        if (editDepto != null && !editDepto.equals("")) {
            listaPorDepto2 = adminUsuarioBO.listPorDepto(editDepto);
        } else {
            deptoSelect = "para que no sea null";
        }
    }

    public void insertCentro() {
        this.sigCentro.setDepartamento(departamento);
        if (adminUsuarioBO.insertCentro(this, sessionbean)) {
            mensaje(1, "El Nuevo Centro ha sido Guardado Existosamente, puede verlo en la Tabla de Abajo!");
            System.out.println(departamento);
            sigCentro = new SigCentro();
            RequestContext context = RequestContext.getCurrentInstance();
            actualizarTabla();
            context.update("formCentro");
            context.update("tableCentros");
            //context.execute("PF('successDesc').show();");
        } else {
            mensaje(2, "Error al Agregar el Centro");
        }
    }
    
    public void actualizarTabla(){
        listCentro = adminUsuarioBO.listCentro();
    }
    public void refreshPage() {
        try {
            FacesContext contex = FacesContext.getCurrentInstance();
            String path = FacesContext.getCurrentInstance().getExternalContext().getContextName();
            contex.getExternalContext().redirect(path + "?faces-redirect=true");
        } catch (Exception e) {
            System.out.println("No se pudo realizar el ingreso!");
        }

    }

    public void updateEstadoRol(SigCentro sigCentro) {
        sigCentro.setEstado(!sigCentro.isEstado());
        if (adminUsuarioBO.updateCentro(sigCentro, sessionbean)) {
            mensaje(1, "Estado del rol modificado");
            listCentro = adminUsuarioBO.listCentroTodos();
        } else {
            mensaje(2, "Error al modificar el estado del centro");
        }
    }

    public void updateEstadoActivo(SigCentro sigCentro) {
        sigCentro.setEstado(!sigCentro.isEstado());
        if (adminUsuarioBO.updateCentro(sigCentro, sessionbean)) {
            mensaje(1, "Estado del Centro Modificado");
            listCentro = adminUsuarioBO.listCentroTodos();
        } else {
            mensaje(2, "Error al modificar el estado del Centro");
        }
    }

    public void onRowEdit(SigCentro sigCentro) {
        adminUsuarioBO.updateCentro(sigCentro, sessionbean);
    }

    public void onRowCancel() {
        mensaje(2, "Modificación cancelada");
    }

    public void centroSeleccionado(SigCentro sigCentro) {
        if (listCentro.size() > 0) {
            for (SigCentro centro : listCentro) {
                centro.setCentroS(false);
                if (centro.getPkIdCentro() == centro.getPkIdCentro()) {
                    centro.setCentroS(true);
                }
            }
        }
    }

}
