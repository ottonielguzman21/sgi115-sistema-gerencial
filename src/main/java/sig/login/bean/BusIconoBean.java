/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.login.bean;

import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.context.RequestContext;
import sig.login.bo.BuscadorBO;
import sig.login.clase.Mensajes;
import sig.login.entity.SigIcono;

/**
 *
 * @author Administrador
 */
@ManagedBean
@ViewScoped
@Getter
@Setter
public class BusIconoBean extends Mensajes {

    @ManagedProperty(value = "#{buscadorBO}")
    private BuscadorBO buscadorBO;

    private SigIcono icono;
    private List<SigIcono> listIcono;
    private String bean;
    private String metodo;
    private String pagina;

    public BusIconoBean() {
    }

    public void limpiar() {
        bean = null;
        metodo = null;
        icono = new SigIcono();
    }

    public void mostrarBusc() {
        if (!(bean == null ? "" : bean).isEmpty() && !(metodo == null ? "" : metodo).isEmpty()) {
            listIcono = buscadorBO.listIcono();
            RequestContext.getCurrentInstance().execute("PF('widIco').show();");
            RequestContext.getCurrentInstance().update("fromAIco");
        }
    }

    public void insertIcono() {
        if (buscadorBO.insertIcono(icono)) {
            icono = new SigIcono();
            listIcono = buscadorBO.listIcono();
            mensaje(1, "Icono guardada corectamente");
        } else {
            mensaje(2, "Error al agregar el icono");
        }
    }

    public void onRowEdit(SigIcono in) {
        if (buscadorBO.updateIcono(in)) {
            mensaje(1, "Icono modificado correctamente");
        } else {
            mensaje(2, "Error al modificar el icono");
        }
    }

    public void onRowCancel() {
        mensaje(2, "Modificación cancelada");
    }

    /*ISBM*/
    public void irPagIcono() {
        try {
            pagina = buscadorBO.consultarParametro(1);
            if (pagina != null) {
//                FacesContext.getCurrentInstance().getExternalContext().redirect("https://www.primefaces.org/showcase/ui/misc/fa.xhtml");
                FacesContext.getCurrentInstance().getExternalContext().redirect(pagina);
            }else{
                mensaje(2, "Error no se encontro la pagina");
            }
        } catch (Exception e) {
            System.out.println("** ERROR EN PAGINA " + e);
        }

    }
}
