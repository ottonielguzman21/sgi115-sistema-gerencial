package sig.login.entity;
// Generated 08-18-2019 03:27:04 PM by Hibernate Tools 4.3.1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Otoniel Guzman
 */
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

@Table(name = "sig_ubicacion", catalog = "sig115")
public class SigUbicacion implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "pk_ubicacion", length = 7, unique = true, nullable = true)
    private int pk_ubicacion;

    @Column(name = "DEPARTAMENTO", nullable = true, length = 100)
    private String DEPARTAMENTO;

    @Column(name = "NOM_DEPTO", nullable = true, length = 100)
    private String NOM_DEPTO;

    @Column(name = "MUNICIPIO", nullable = false, length = 100)
    private String MUNICIPIO;

    @Column(name = "NOM_MUN", nullable = false, length = 100)
    private String NOM_MUN;

    public SigUbicacion(int pk_ubicacion) {
        this.pk_ubicacion=pk_ubicacion;
    }
    @Transient
    public String deptoSelect;
}
