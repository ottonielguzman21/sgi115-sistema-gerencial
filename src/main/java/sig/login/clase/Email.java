/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.login.clase;

/**
 *
 * @author Sanlegas
 */
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Email {

    String usuarioCorreo = "sicacir.help@gmail.com";
    String password = "ROOT2019";
    List<InternetAddress> listDestinatarios = new ArrayList<>();
    String asunto;
    String mensaje;
    String rutaArchvio;

    public Email(String usuarioCorreo, String password, List<InternetAddress> listDestinatarios, String asunto, String mensaje) {
        this.usuarioCorreo = usuarioCorreo;
        this.password = password;
        this.listDestinatarios = listDestinatarios;
        this.asunto = asunto;
        this.mensaje = mensaje;
    }


    public boolean sendMail() {
        try {
            if (usuarioCorreo != null && password != null && listDestinatarios.size() > 0) {
                Properties props = new Properties();
                props.put("mail.smtp.auth", "true");
                props.put("mail.smtp.starttls.enable", "true");
                props.put("mail.smtp.host", "smtp.gmail.com"); //CORREO PUBLICO
                props.setProperty("mail.smtp.port", "587");    //CORREO PUBLICO
                props.put("mail.smtp.user", usuarioCorreo);
                props.put("mail.smtp.password", password);

                Session session = Session.getInstance(props, null);

                BodyPart cuerpoMensaje = new MimeBodyPart();
                cuerpoMensaje.setText(mensaje);

                MimeMultipart multiParte = new MimeMultipart();
                multiParte.addBodyPart(cuerpoMensaje);

                MimeMessage message = new MimeMessage(session);
                message.setFrom(new InternetAddress(usuarioCorreo));
//                message.addRecipient(Message.RecipientType.TO, new InternetAddress(destinatario));
                InternetAddress[] iAdressArray = new InternetAddress[listDestinatarios.size()];
                iAdressArray = listDestinatarios.toArray(iAdressArray);
//                message.addRecipients(Message.RecipientType.CC, iAdressArray );
                message.addRecipients(Message.RecipientType.TO, iAdressArray);
                message.setSubject(asunto);
                message.setContent(multiParte);

                /*ARCHIVO*/
                if (rutaArchvio != null) {
                    cuerpoMensaje = new MimeBodyPart();
                    DataSource fuente = new FileDataSource(rutaArchvio);
                    cuerpoMensaje.setDataHandler(new DataHandler(fuente));
                    cuerpoMensaje.setFileName(rutaArchvio);
                    multiParte.addBodyPart(cuerpoMensaje);
                }
                //***************

//                Transport.send(message);
                Transport t = session.getTransport("smtp");
                t.connect(usuarioCorreo, password);
                t.sendMessage(message, message.getAllRecipients());
                t.close();

                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

}
