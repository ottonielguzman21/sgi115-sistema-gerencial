/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sig.login.clase;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 *
 * @author Administrador
 */
@Getter
@NoArgsConstructor
public class Mensajes implements Serializable{
        
    private FacesMessage message;
    
    public void mensaje(int val,String text){
        switch(val){
            case 1://Información
                message = new FacesMessage(FacesMessage.SEVERITY_INFO, text, "");
                FacesContext.getCurrentInstance().addMessage(null,message);
            break;
            case 2://Error
                message = new FacesMessage(FacesMessage.SEVERITY_FATAL, text, "");
                FacesContext.getCurrentInstance().addMessage(null,message);
            break;
            case 3:
                message = new FacesMessage(FacesMessage.SEVERITY_WARN, text, "");
                FacesContext.getCurrentInstance().addMessage(null,message);
            break;
        }
    }
    
}
